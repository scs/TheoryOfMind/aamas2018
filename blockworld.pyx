#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  1 14:39:49 2017

A simple representation for 2D blockworld environments.

Rename to blockworld.pyx when using cython to improve performance a little.

@author: jpoeppel
"""

#Use deque instead of queue for performance reasons
from collections import deque

from operator import attrgetter

import heapq

import numpy as np

colors = {"a": "", "g": "", "#": "rgba(96,96,96,1)", "t": "green"}

passables = {"a": True, "g": True, "#": False, "t": True}


class DefaultDict(dict):
    
    def __init__(self, factory):
        self.factory = factory
        
    def __missing__(self, key):
        self[key] = self.factory(key)
        return self[key]

class Tile(object):
    """
        Simple representation of a single tile within a blockworld.
    """
    
    def __init__(self):
        self._passable = True
        self._f = 0
        self._g = 0
        self._h = DefaultDict(None)
        self.neighbours = set([])
        self.pos = ()
        self.color = "" 
        self.targetColor = ""
        self.isTarget = False
        self.symbol = ""
    

    def to_dict(self, targetVisible=False):
        """
            Gives a simplified dictionary (allows easy json conversion) of
            the tile.
            
            Parameters
            ---------
            targetVisible: bool (default=False)
                Specifies if this tile is seen as a target which changes which
                color is to be shown.
            
            Returns
            ------
                dict
                A dictionary containing the "passable", "color" and "symbol"
                keys. If targetVisible is set to true, the color will be
                the target color instead of the normal color.
        """
        if targetVisible:
            return {"passable": self.passable, 
                    "color": self.targetColor, 
                    "symbol": self.symbol}
        return {"passable": self.passable, 
                "color": self.color, 
                "symbol": self.symbol}
    
    @classmethod
    def invisible(cls):
        #Passable: False is due to the js implementation currently
        """
            Classmethod to return a generic "invisible" tile.
            The passable is None to represent "unknown", which requries special
            care when dealing with this elsewhere. 
        """
        return {"passable": None, "color": "black", "symbol": ""}
    
    
    def __gt__(self,other):
        return self.pos[0] > other.pos[0] or \
                (self.pos[0] == other.pos[0] and self.pos[1] > other.pos[1])

    def __repr__(self):
        return str(self.pos)
    
class Environment(object):
    """
        A simple representation of 2D gridworlds.
    """
    
    def __init__(self, envString=None, alwaysVisibles=None, visibleColor=""):
        """
            Constructor for the environments.
            
            Parameters
            ---------
            envString: string (optional)
                An optional string representing the gridworld which will be
                parsed upon creation if given.
            alwaysVisibles: list (optional)
                Stores tile position which should always be visible, regardless
                of these tiles being within the Area of View or not.
            visibleColor: string (optional)
                A string representation of the color of always visible tiles.
        """
        self.tiles = {}
        self.size = (0,0)
        if not alwaysVisibles:
            alwaysVisibles = []
        self.alwaysVisibles = alwaysVisibles
        self.visibleColor = visibleColor
        if envString:
            self.parse_environment(envString)
            
            
        self.mpaa_initialized = False
        self.dist_memory = {}
        self.est_dist_mem = {}
        self.vis_memory = {}
        self.obs_memory = {}
        self.visible_memory = set([])
        
    def parse_environment(self, envString):
        """
            Function to parse an environment string, setting up the tiles
            accordingly.
            
            Parameters
            ----------
            envString: string
                A string containing ascii symbols representing the environemnt.
                Currently the symbols must be those contained within the
                global "passables" and "colors" dictionaries!
        """
        for i, row in enumerate(envString.split("\n")):
            for j, element in enumerate(row):
                tile = Tile()
                tile.pos = (i,j)
                tile.passable = passables[element]
                tile.color = colors[element]
                self.tiles[tile.pos] = tile
        """ """ """ """  """ """ """ """""" """  """ """
        # Not ideal way, but works since the string represents
        # the world from top left to bottom right
        maxPos = tile.pos 
        # Add all neighbours
        for tile in self.tiles.values():
            for i,j in [(-1,0),(1,0),(0,-1),(0,1)]:
                newPos = (min(max(tile.pos[0]+i, 0), maxPos[0]), 
                          min(max(tile.pos[1]+j,0),maxPos[1]))
                tile.neighbours.add(self.tiles[newPos])
        self.size = (maxPos[0]+1,maxPos[1]+1)
        
    def set_targets(self, targets):
        """
            Function to designate certain tiles as special targets, which sets
            additional attributes of these tiles.
            
            Parameters
            ----------
            targets: dict
                A dictionary of dictionaries containing the target positions 
                as keys with another dictionary as value for each target
                containing color and symbol information about this target.
        """
        for t in targets:
            self.tiles[t].isTarget = True
            self.tiles[t].targetColor = targets[t]["color"]
            self.tiles[t].symbol = targets[t]["symbol"]
        
    def to_simple_list(self, agentPos=None, viewRadius=None, targetRadius=None):
        """
            Creates a list of dictionaries representation (suitable for json)
            of the environment. Can "hide" parts of the environemnt with
            invisible tiles outside the Area of View around a given position.
            
            Parameters
            ----------
            agentPos: tuple (optional)
                The position around which the Area of View is to be computed.
            viewRadius: int (optional)
                The vision radius for the Area of View
            targetRadius: int (optional)
                A second vision radius, determining the distance where one
                can see targets (see handle_octants). Is assumed to be smaller
                than the viewRadius!
                
            Returns
            -------
                list of dicts
                A list of dictionaries for each tile in the environment. By 
                default, the entire environment is returned as it is. If the
                parameters are given, Tiles not in the Area of View will be
                replaced by generic "invisible" tiles.
        """
        res = []
        if not viewRadius and not targetRadius:
            # Shortcut when we do not need to deal with lines of sight
            for i in range(self.size[0]):
                tmp = []
                for j in range(self.size[1]):
                    if (i,j) in self.alwaysVisibles:
                        tmp.append({"passable":True, 
                                    "color":self.visibleColor, 
                                    "symbol":""})
                    else:
                        tmp.append(self.tiles[(i,j)].to_dict())
                res.append(tmp)
        else:
            if agentPos is None:
                raise ValueError("Requires agentPos when view Radius is given!")
            # Compute Visibles
            visiblesMap = [(i,j) for i in range(self.size[0]) for j in 
                               range(self.size[1])] if not viewRadius else []
            visiblesTargets = []
            for octant in range(8):
                tmpInner, tmpOuter = self.handle_octant(agentPos, octant, 
                                                    radiusInner=targetRadius, 
                                                    radiusOuter=viewRadius)
                visiblesTargets += tmpInner
                visiblesMap += tmpOuter
                
            if self.alwaysVisibles:
                visiblesMap.extend(self.alwaysVisibles)
                
            visiblesMap = set(visiblesMap)
            visiblesMap.add(agentPos)
            
            visiblesTargets = set(visiblesTargets)
            visiblesTargets.add(agentPos)
            for i in range(self.size[0]):
                tmp = []
                for j in range(self.size[1]):
                    #TODO Consider making this more efficient,
                    # by not going over visibles every time!
                    if (i,j) not in visiblesMap:
                        tmp.append(Tile.invisible())
                    else:
                        if self.tiles[(i,j)].isTarget and \
                                                    (i,j) in visiblesTargets:
                                                        
                            tmp.append(self.tiles[(i,j)].to_dict(
                                                        targetVisible=True))
                        elif (i,j) in self.alwaysVisibles:
                            tmp.append({"passable":True, 
                                        "color":self.visibleColor, 
                                        "symbol": ""})
                        else:
                            tmp.append(self.tiles[(i,j)].to_dict())
                res.append(tmp)
        return res
            
    def handle_octant(self, agentPos, octant, radiusInner=None, 
                                                          radiusOuter=None):
        r"""
            Computes the visible tiles within the given octant.
            
            Visibility algorithm adapted from:
            "https://blogs.msdn.microsoft.com/ericlippert/2011/12/12/
            shadowcasting-in-c-part-one/"
            
            However, I set up the octants as follows:
                                  
                               \ 5|6 / 
                              4 \ | / 7
                             ----------- 
                              3 / | \ 0
                               / 2|1 \
                               
            Parameters
            ---------
            agentPos: tuple
                The agent position from which the octant is to be processed.
            octant: int
                The number of the currently processed octant.
            radiusInner: int 
                Inner (minimum) visible radius. 
            radiusOuter: int
                Outer (maximum) visible radius.
                
            Returns
            -------
            innerVisibilities: list
                List of Tiles visible in the inner radius within the given
                octant. Will be empty if radiusInner is not given.
            outerVisibilities: list
                List of Tiles visible in the outer radius within the given
                octant. Will be empty if radiusOuter is not given.
                
        """
        taskdeque = deque()
        # An item consists of: Column, Toprow, bottomrow
        visiblesInner = []
        visiblesOuter = []
        firstItem = (1, 1, 0)
        taskdeque.append(firstItem)
        while True:
            try:
                curItem = taskdeque.popleft()
            except IndexError:
                break
            curColumn, top, bot = curItem
            tmpInner, tmpOuter = self.handle_column(agentPos, curColumn, top, 
                                                    bot, taskdeque, octant, 
                                                    radiusInner, radiusOuter)
            visiblesInner += tmpInner
            visiblesOuter += tmpOuter
        return visiblesInner, visiblesOuter
    
    def handle_column(self, agentPos, col, topSlope, botSlope, tasks, octant, 
                      radiusInner=None, radiusOuter=None):
        """
            Computes the visible tiles within the given column of the 
            given octant. Can distinguish between two different vision
            radii, e.g. to distinguish seeing walls and identifying more 
            details about blocks.
            
            Parameters
            ----------
            agentPos: tuple
                The position of the agent in the tile from which the visibility
                is to be computed.
            col: int
                The current column to be processed
            topSlope: float
                The current slope towards the highest still valid tile within
                the given column.
            botSlope: float
                The current slope towards the lowest still valid tile wihin
                the given column.
            tasks: list
                List of tasks (i.e. columns and slopes) still to process
            octant: int
                The currently processed octant.
            radiusInner: int 
                Inner (minimum) visible radius. 
            radiusOuter: int
                Outer (maximum) visible radius.
                
            Returns
            -------
            innerVisibilities: list
                List of Tiles visible in the inner radius within the given
                column. Will be empty if radiusInner is not given.
            outerVisibilities: list
                List of Tiles visible in the outer radius within the given
                column. Will be empty if radiusOuter is not given.
                
        """
        
        switchRes = False
        if radiusInner and not radiusOuter:
            radiusOuter = radiusInner
            radiusInner = None
            switchRes = True
        if radiusOuter:
            if col > radiusOuter:
                return [], []
        # Ignore inverted columns and columns that are too small
        if topSlope < botSlope or abs(topSlope-botSlope)*col < 0.001:
            return [], []
        
        
        colLower = col-0.5
        
        botRow = colLower*botSlope # Consider left edge for bot row
        if botRow - int(botRow) > 0.5:
            botRow = int(botRow+1)
        else:
            botRow = int(botRow)
        topRow = (col+0.5)*topSlope
        if topRow - int(topRow) > 0.5:
            topRow = int(topRow+1)
        else:
            topRow = int(topRow)
        worldShape = self.size
        # Make sure topRow is still within bounds
        topRow = min(topRow, worldShape[0]-1) 
        visiblesInner = []
        visiblesOuter = []
        lastRowTransparent = None
        onlyEdges = False
        
        
        colSquare = colLower*colLower
        
        for row in range(botRow, topRow+1):
            rowLower = row-0.5
            rowLowerSquare = rowLower*rowLower
            # Break if we are outside the radius
            if radiusOuter != None and \
                        rowLowerSquare+colSquare > radiusOuter*radiusOuter:
                break
            
            pos = self._transform_octant(octant, agentPos, row, col)
            # Break if we are outside the world dimensions, ask for forgiveness
            try:   
                currentTransparent = self.tiles[pos].passable
            except KeyError:
                #We must be outside our target area
                break
            # Add current tile to the visible positions
            # In order to show technically invisible edges, check for the
            # onlyEdges variable. If this is set, we only add non-transparent
            if radiusInner != None and \
                        rowLowerSquare+colSquare <= radiusInner*radiusInner:
                if not onlyEdges or not currentTransparent:
                    visiblesInner.append(pos)
            if not onlyEdges or not currentTransparent:
                visiblesOuter.append(pos)
            
            # Check if we need to update slopes
            if not currentTransparent:
                if lastRowTransparent:
                    # Create new section
                    newTopSlope = rowLower/(col+0.5)
                    newItem = (col+1, newTopSlope, botSlope)
                    tasks.append(newItem)
                    
                botSlope = (row+0.5)/colLower
                if not botSlope < topSlope:
                    onlyEdges = True
                    
            lastRowTransparent = currentTransparent
            
        if lastRowTransparent != None and lastRowTransparent:
            newItem = (col+1, topSlope, botSlope)
            tasks.append(newItem)
            
        return (visiblesInner, visiblesOuter) if not switchRes else \
                                                (visiblesOuter, visiblesInner)
                    
            
    def _transform_octant(self, octant, agentPos, row, col):
        """
            Warpes the agentPosition so that it corresponds to the axis of the
            given octant.
            
            Parameters
            ---------
            octant: int
                The number of the given octant, see "handle_octant" for their
                placement.
            agentPos: tuple
                The position to be transformed.
            row: int
                The currently considered row in the octant
            col: int
                The currently considered column in the octant
                
            Returns
            -------
                tuple
                The transformed agent position.
        """
        ax, ay = int(agentPos[0]), int(agentPos[1])

        #Quite costly as it takes around 0.1 second on 1 trail    
        if octant == 0:
            return (ax+row, ay+col)
        elif octant == 1:
            return (ax+col, ay+row)
        elif octant == 2:
            return (ax+col, ay-row)
        elif octant == 3:
            return (ax+row, ay-col)
        elif octant == 4:
            return (ax-row, ay-col)
        elif octant == 5:
            return (ax-col, ay-row)
        elif octant == 6:
            return (ax-col, ay+row)
        else:
            return (ax-row, ay+col)
        
    def is_visible(self, position, curPos, radius=3, remember=False):
        """
            Checks if the given position is currently visible from the 
            given current position.
            
            Parameters
            ----------
            position: tuple
                The position to check
            curPos: tuple
                The current position from where to check
            radius: int (default=3)
                The radius in which tiles should be visible around a given
                position.
            remember: bool (default=False)
                If True, observed tiles will be remembered, i.e. the 
                visible_memory will be updated.
                
            Returns
            -------
                bool
                True if the given position is visible from the current position
                given the radius, False otherwise.
        """
        
        # vis_memory is a lookup to speed up successive calls to this function
        # and should not be confused with visible_memory!
        if (curPos, position, radius) in self.vis_memory:
            return self.vis_memory[(curPos, position, radius)]
        
        x, y  = (position[0] - curPos[0]), (position[1] - curPos[1])
        
        # Determine the octant to check for visibility so that we do not need
        # to check the entire circle around us
        octant = [([1, 0], [2, 3]), ([6, 7], [5, 4])][x < 0][y < 0][abs(x) < abs(y)]
        visibles, _ = self.handle_octant(curPos, octant, radius)

        if remember:
            for p in visibles:
                self.visible_memory.add(p)

        for p in visibles:
            self.vis_memory[(curPos, p, radius)] = True
            
        if not position in visibles:
            self.vis_memory[(curPos, position, radius)] = False

        return position in visibles
    
    def was_visible(self, position):
        """
            Helper function to check if a position has already been seen this
            run.
            
            Parameters
            ----------
            position: tuple
                The position to check
                
            Returns
            -------
                bool
                True if the position has already been seen (i.e. has been 
                stored in visible_memory), False otherwise.
        """
        return position in self.visible_memory
    
    
    def simulate_MPAA(self, start, end, visionRadius=3):
        """
            Uses the Multipath Adaptive A* algorithm by Hernandez et. al 2014
            ([https://www.aaai.org/ocs/index.php/ICAPS/ICAPS14/paper/view/7944])
            to compute a path from the start to the end under
            the freespace assumption for all tiles not present in 
            self.visible_memory tiles. This will simulate navigating the 
            environment while continuously updating the internal representation
            of the enviornment.
            
            Parameters
            ---------
            start: tuple
                Start postiion
            end: tuple
                End position
            visionRadius: int (default=3)
                The radius with which new observations are made around the 
                current position.
                
            Returns
            ------
                list
                The list of positions from start to end, which the MPAA
                algorithm found as the shortest path under the freespace 
                assumption. If no path can be found, returns None
        """
        self._observe(start, visionRadius) #Modifies visible_memory
        self._init_MPAA(end)
            
        cur = self.tiles[start]
        traj = [start]
        while cur.pos != end:
            self.counter += 1
            newState, closed = self._astar(cur, self.tiles[end])
            if newState is None:
                print("Goal {} not reachable from {}".format(end, cur))
                return None
            
            self._update_heuristic(newState, closed)
            
            self._build_path(newState, cur)
            updateWalls = False
            while not updateWalls and cur.pos != end:
                t = cur
                cur = cur._next
                t._next = None
                #Modifies visible_memory
                updateWalls = self._observe(cur.pos, visionRadius) 
                traj.append(cur.pos)
        
        return traj
    
    
    def compute_distance_partially_visible(self, start, end, observe=True,
                                                               visionRadius=3):
        """
            Function which computes the distance between the given start and
            end positions under the free space assumption, using the MPAA 
            algorithm proposed by Hernandez et. al 2014 (see simulate_MPAA).
            
            Parameters
            ----------
            start: tuple
                Start position 
            end: tuple
                End position
            observe: bool (default=True)
                If True, will update the partial visibility by observing the 
                environment in a radius around the given start position.
            visionRadius: int (default=3)
                In case observe is True, specifies the radius of the new
                observation.
                
            Returns
            -------
                int
                The distance from the start to the end postiion, if a way can
                be found, otherwise None.
        """
        
        if start == end:
            return 0
        
        if not self.mpaa_initialized:
            self._init_MPAA(end)
            
        if observe:
            self._observe(start, visionRadius)
        
        self.counter += 1
        
        newState, closedList = self._astar(self.tiles[start], self.tiles[end])
        if newState is None:
            print("Goal {} not reachable from {}".format(end, start))
            print("counter: ", self.counter)
            return None
        
        self._update_heuristic(newState, closedList)
        
        self._build_path(self.tiles[start], newState)
        
        return newState._f
        
    
    def _init_MPAA(self, goal):
        """
            Initializes all Tile objects for use in the MPAA algorithm.
            
            Parameters
            ----------
            goal: tuple
                Goal position
        """
        for tile in self.tiles.values():
            tile._search = 0
            tile._h = self._estimated_distance(tile.pos, goal)
            tile._next = None
            tile._parent = None
        
        self.counter = 0
        self.mpaa_initialized = True
        
        
    def _update_heuristic(self, goalState, closedList):
        """
            The update heuristic function proposed by Hernandez et. al 2014.
            Must only be used after running the _astar algorithm.
            
            Parameters
            ----------
            goalState: tile
                The Tile object for the goal state
            closedList: list
                The list of all the Tile objects that have been processed in
                the last run of the modified _astar.
        """
        for tile in closedList:
            #Update heuristic
            tile._h = goalState._f - tile._g
        
    def _build_path(self, start, goal):
        """
            Connects the start and goal tile. Must only be run, after the
            _astar function was run so that the used attributes have been set
            correctly.
            
            Parameters
            ---------
            start: tile
                The tile object for the starting postiion
            goal: tile
                The tile object for the goal position.
        """
        t = goal
        while t.pos != start.pos:
            t._parent._next = t
            t = t._parent
            
    def _observe(self, curPos, radius=3):
        """
            Private function to update the partial visibility by observing the
            environment in the given radius around the given position.
            
            Parameters
            ---------
            curPos: tuple
                The position from where to observe
            radius: int
                The radius with which to observe (default 3)
            
            Returns
            ------
                bool
                True if new walls were detected, False otherwise.
        """
       
        if (curPos, radius) not in self.obs_memory:
            visibles = []
            for octant in range(8):
                tmpInner, _ = self.handle_octant(curPos, octant, 
                                                         radiusInner=radius)
                visibles += tmpInner
                
            visibles = set(visibles)
            self.obs_memory[(curPos, radius)] = visibles
            
        visibles = self.obs_memory[(curPos, radius)]
        
        alreadySeen = self.visible_memory
        updateWalls = False
        for t in visibles-alreadySeen:
            if not self.tiles[t].passable:
                updateWalls = True
                for n in self.tiles[t].neighbours:
                    n._next = None
            alreadySeen.add(t)
        return updateWalls
            
                
    def _astar(self, start, end):
        """
            An extended version of the A* algorithm suitable for only partially
            observable/visible environments. Changes were made according to 
            Hernandez et. al 2014
            ([https://www.aaai.org/ocs/index.php/ICAPS/ICAPS14/paper/view/7944])
            
            Parameters
            ----------
            start: tuple
                Start position
            end: tuple
                End postiion
                
            Returns
            -------
                tile, list or None, []
                The tile corresponding to the goal state and the list of tiles
                that were already processed. If the goal cannot be found or 
                reached, None is returned.
        """
        
        alreadySeen = self.visible_memory
        counter = self.counter
        
        #Initialize start
        if start._search != counter:
            start._g = np.inf
            
        start._search = counter
        start._parent = None
        start._g = 0
        start._f = start._g + start._h
        entry = [start._f, start, 1]
        openList = []
        heapq.heappush(openList, entry)
        mapping = {start.pos: entry}
        closedList = set([])
        while openList:
            while openList:
                _, curUnit, valid = heapq.heappop(openList)
                if valid:
                    break
                
            #Check goal condition, updated for mpaa
            t = curUnit
            while (not t._next is None) and (t._h == t._next._h + 1):
                t = t._next
            if t.pos == end.pos:
                return curUnit, closedList
            
            closedList.add(curUnit)
            
            for neighbour in curUnit.neighbours:
                # Free space assumption
                nPassable = neighbour.passable if neighbour.pos in \
                                                    alreadySeen else True
                if not nPassable or neighbour in closedList:
                    # Continue with the next neighbour
                    continue
                
                #Initialize neigbour
                if neighbour._search != counter:
                    neighbour._g = np.inf
                neighbour._search = counter
                
                if neighbour._g > curUnit._g + 1:
                    neighbour._g = curUnit._g + 1
                    neighbour._parent = curUnit
                    if neighbour.pos in mapping:
                        mapping[neighbour.pos][-1] = 0
                    neighbour._f = neighbour._g + neighbour._h

                    entry = [neighbour._f, neighbour, 1]
                    mapping[neighbour.pos] = entry
                    heapq.heappush(openList, entry)
                
        return None, []
        
    def compute_distance(self, start, end):
        """
            Computes the distance between start and end using the A* algorithm.
            
            Parameters
            ----------
            start: tuple
                Start position for the A*
            end: tuple
                Goal position
                
            Returns
            -------
                int or None
                The distance from the start to the end position if a way can
                be found, otherwise None
        """
        if (start,end) in self.dist_memory:
            return self.dist_memory[(start,end)]
        # Reset a* variables
        for tile in self.tiles.values():
            tile._f = 0
            tile._g = 0
            tile._came_from = None
        startUnit = self.tiles[start]
        if not startUnit.passable:
            return None
        openList = [startUnit]
        closedList = set([])
        while len(openList) != 0:
            sList = sorted(openList, key=attrgetter("_f"))
            curUnit = sList[0]
            # Check goal condition
            if curUnit.pos == end:
                self.dist_memory[(start,end)] = curUnit._f
                return curUnit._f
                
            openList = sList[1:]
            closedList.add(curUnit)
            for neighbour in curUnit.neighbours:
                if not neighbour or not neighbour.passable or neighbour in closedList:
                    continue
                # Current goal costs
                gCosts = curUnit._g + 1 # Assume equal costs for each transition for now
                if neighbour in openList and gCosts >= neighbour._g:
                    continue
                
                # Uodate neighbours costs
                neighbour._g = gCosts
                if neighbour in openList:
                    openList.remove(neighbour)
                    
                neighbour._f = gCosts+self._estimated_distance(neighbour.pos, end)
                neighbour._came_from = curUnit
                openList.append(neighbour)
        return None
    
    def get_shortest_trajectory(self, start, end):
        """
            Computes the shortest path from start to end, making use of the 
            A* algorithm in "compute_distance".
            
            Parameters
            ----------
            start: tuple
                The start position for the trajectory.
            end: tuple
                The goal position for the trajectory.
                
            Returns
            -------
                list, int
                A list of positions from the start to the goal position as 
                well as the overall distance.                
        """
        dist = self.compute_distance(start, end)
        traj = [end]
        cur = self.tiles[end]._came_from
        while not cur is None:
            traj.append(cur.pos)
            cur = cur._came_from
        return traj[::-1], dist
    
    def _estimated_distance2(self, start, end):
        """
            Heuristic for the A* algorithm, estimating the distance between
            to positions as the euklidean distance.
            
            Parameters
            ----------
            start: tuple
                Start position
            end: tuple
                End position
                
            Returns
            -------
                float
                Estimated distance between the two given positions.
        """
        if (start,end) in self.est_dist_mem:
            return self.est_dist_mem[(start,end)]
        dif = (start[0]-end[0], start[1]-end[1])
        dist = np.sqrt(np.dot(dif,dif))
        self.est_dist_mem[(start,end)] = dist
        return dist

    def _estimated_distance(self, start, end):
        """
            Heuristic for the A* algorithm, estimating the distance between
            to positions as the Manhattan distance.
            
            Parameters
            ----------
            start: tuple
                Start position
            end: tuple
                End position
                
            Returns
            -------
                float
                Estimated distance between the two given positions.
        """
        if (start,end) in self.est_dist_mem:
            return self.est_dist_mem[(start,end)]
        (x1, y1) = start
        (x2, y2) = end
        dist = abs(x1 - x2) + abs(y1 - y2)
        self.est_dist_mem[(start,end)] = dist
        return dist
    
    
if __name__ == "__main__":
    envString = "######################\n" + \
                "#gggggggggggggggggggg#\n" + \
                "#g#g###g#g#g#g###g####\n" + \
                "#g#ggg#ggg#g#ggg#gggg#\n" + \
                "#g###g#####g###g#g##g#\n" + \
                "#g#ggg#ggg#g#gggggggg#\n" + \
                "#g#g###g###g#g###g####\n" + \
                "#ggggggg#ggg#gg##gggg#\n" + \
                "#g#######g#g#g##gg##g#\n" + \
                "#ggggggg###g###gg###g#\n" + \
                "#g#####gg##g##gg##g#g#\n" + \
                "#ggggg##gg#g#gg##gggg#\n" + \
                "#g###g###g#g#g#####gg#\n" + \
                "#ggggggggggggggg#gggg#\n" + \
                "######################\n"
    start = (8,9)
    end = (5,9)
    env = Environment(envString)
    # env.is_visible((6,11), start)
    print(env.compute_distance(start, end))
