#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 1 15:36:04 2017
    Simple agent which replays the actions recorded during the 
    webblocks experiment.
@author: jpoeppel
"""

import datetime
import os
from ast import literal_eval

import threading

from blockworld import Environment

CONDITION_PATH = os.path.join(os.path.dirname(__file__), "../Conditions")

# Global memory to speed up parsing for subsequent occurances of the same
# condition.
env_store = {}

def crawl_results(path, use_caching=True, get_participants=False):
    """
        Collects and reads in all the recorded participant behaviour at the 
        given root path.

        Parameters
        ----------
        path: str
            The root path where to find the participant recordings.
        use_caching: bool (default= True)
            If true, will reuse already loaded environments, which may already
            include precomputed distances, greatly speeding up later calls,
            but might invalidate timing analysis.
        get_participants: bool (default=False)
            If true, will return a dictionary with participant ids as keys and
            their encountered conditions as values.

        Returns
        -------
            dict
            A dictionary containing a list of tuples for each recorded run 
            found under the given path of the condition specified by the key,
            each containing the information provided by load_experiment. 
    """
    users = [e for e in os.listdir(path) 
                        if os.path.isdir(path + os.path.sep +e)]
    conditions = {}
    skipped = []
    num_completed = 0
    participants = {}
    for u in users:
        files = os.listdir(path + os.path.sep +u)
        conditionNames = []
        for f in files:
            if "condMap" in f:
                conditionNames.append(f)
                try:
                    exp = load_experiment(path + os.path.sep +u + os.path.sep + f, 
                                                                    use_caching)
                    if exp[0] is not None:
                        conditions[f].append(exp)
                        num_completed += 1
                    else: 
                        skipped.append(u + os.path.sep + f)
                except KeyError:
                    conditions[f] = [load_experiment(path + os.path.sep 
                                                        + u + os.path.sep + f,
                                                        use_caching)]
                    num_completed += 1
                except IndexError:
                    #Ignore bad files
                    pass
        participants[u] = conditionNames
                
    print("Skipped: {} runs because they were incomplete.".format(len(skipped)))
    print("Total number of runs: {}".format(num_completed))
    if get_participants:
        return conditions, participants
    else:
        return conditions


def load_experiment(path, use_caching=True):
    """
        Small function which reads in an experimental result and constructs
        the environment as well as the playback agent from it.

        Parameters
        ----------
        path: str
            The path for the recorded run.
        use_caching: bool (default= True)
            If true, will reuse already loaded environments, which may already
            include precomputed distances, greatly speeding up later calls,
            but might invalidate timing analysis.

        Returns
        -------
            environment: blockworld.Environment
                An environment object corresponding to the world of the 
                experiment.
            targets: dict
                A dictionary containing information for each potential target
                within the environment and condition, with positions as keys.
            playback_agent : PlaybackAgent
                An instance of a PlaybackAgent which can be used to reproduce
                the recorded behaviour.
            goal_pos: tuple
                The position of the true goal for the recorded condition.

    """
    with open(path, "r") as condition:
        lines = condition.readlines()
        condition_end = lines.index("\n")
        
    for i, line in enumerate(lines[:condition_end]):
        if "EnvString" in line:
            start = i+1
        if "AlwaysVisibles" in line:
            end = i
            # break
        if "StartPosition" in line:
            start_pos = literal_eval(line.split(":")[1].strip())
        if "Goal" in line:
            goal = literal_eval(line.strip("Goal:").strip())
        
    env_string = "".join(lines[start:end])
    if not use_caching or env_string not in env_store:
        env_store[env_string] = Environment(env_string)
    environment = env_store[env_string]
    # start_pos = literal_eval(lines[condition_end-1].split(":")[1].strip())
    agent_id = os.path.basename(os.path.dirname(path)) 
    playback_agent = PlaybackAgent(agent_id,
                                  lines[condition_end+1:], 
                                  start_pos=start_pos,  
                                  environment=environment)
    for action in playback_agent.actions:
        if "Finished" in action[1]:
            break
    else:
        #No finished found => skip this experiment.
        return None, None, None, None
    
    # goal = literal_eval(lines[condition_end-2].strip("Goal:").strip())
    # Determine possible goals. Required since C1 and C3 do not store all
    # targets in their files:
    condID = os.path.basename(os.path.normpath(path))
    cond2ID = condID.split("_")[0] + "_C2_" + condID.split("_")[-1]
    with open(CONDITION_PATH + os.path.sep + cond2ID, "r") as condition2:
        for line in condition2:
            if "Targets:" in line:
                targets = literal_eval(line[line.find("{"):])
                break
            
    return environment, targets, playback_agent, goal["target"]
        


class PlaybackAgent(object):
    """
        Simple playback agent which will reproduce the the recorded actions
        of the participants.
    """
    
    def __init__(self, agent_id, action_rows, start_pos, environment):
        """
            Constructor for the playback agent

            Parameters
            ----------
            agent_id: str/int
                The identifier for the agent.
            action_rows: str
                The string containing all the performed actions that were 
                recorded.
            start_pos: tuple 
                The agent's starting position.
            environment: blockworld.Environment
                A reference to the environment object corresponding to the world
                the actions were recorded in.
        """
        self.id = agent_id
        try:
            self.actions = self._parse_actions(action_rows)
        except IndexError:
            raise IndexError("Error parsing actions of user: {}".format(agent_id))
        self.environment = environment
        self.curIdx = 0
        self.start_pos = start_pos
        self.pos = start_pos


        self.running = False
        self.startTime = self.actions[0][0]
        self.lastTimeStamp = self.startTime
    
    def reset(self):
        self.pos = self.start_pos
        self.curIdx = 0

        
    def _parse_actions(self, action_rows):
        """
            Private function to parse the action recordings.

            Parameters
            ----------
            action_rows: str
                The string containing all the performed actions that were 
                recorded.

            Returns
            -------
                list
                A list of tuples containing, the timestamp of the action and
                the action string.
        """
        res = []
        date_split = action_rows[0].find(": ")-1
        for row in action_rows:
            if row != "":
                res.append((datetime.datetime.strptime(row[:date_split], 
                                                       "%Y-%m-%d %H:%M:%S.%f"), 
                            row[date_split+3:].strip()))
        return res
        
    
    def non_dud_actions(self):
        """
            Extracts the number of actions that actually changed the 
            participants's position from the performed actions.

            Returns
            ------
                int
                The number of actions that change the position.
        """
        agent_pos = self.start_pos
        num_actions = 0
        for action in self.actions:
            if not "Finished" in action[1]:
                _, action = action[1].split("-")
            
                if action == "Left":
                    new_pos = (agent_pos[0], agent_pos[1]-1)
                elif action == "Right":
                    new_pos = (agent_pos[0], agent_pos[1]+1)
                elif action == "Up":
                    new_pos = (agent_pos[0]-1, agent_pos[1])
                elif action == "Down":
                    new_pos = (agent_pos[0]+1, agent_pos[1])
                elif action == "Interaction":
                    continue
                    
                if self.environment.tiles[new_pos].passable:
                    agent_pos = new_pos
                    num_actions += 1
            else:
                break
        return num_actions
        
    def get_positions(self, ignore_duds=False):
        """
            Returns a list of the positions this agent visited.

            Parameters
            ---------
            ignore_duds: bool (default=False)
                If true, actions not changing the agent's position, will be 
                skipped, meaning that any consequtive element in the returned
                list will be different from the previous one.

            Returns
            -------
                [tuple,]
                A list of all the positions this agent visited.
        """

        res = [self.start_pos]
        tmp_pos = self.pos
        self.pos = self.start_pos
        tmp_idx = self.curIdx
        self.curIdx = 0
        while True:
            new_pos = self.perform_action(ignore_duds=ignore_duds)
            if new_pos is None:
                self.curIdx = tmp_idx
                self.pos = tmp_pos
                return res
            else:
                res.append(new_pos)
        
    def perform_action(self, ignore_duds=False, callback=None, speedup=1):
        """
            Trigger the agent to perform the next action.

            Parameters
            ----------
            ignore_dus: bool (default=False)
                If true, actions not changing the agent's position, will be
                skipped.

            Returns
            -------
                tuple
                The new position after performing the action or none, if there
                are no more actions in the recording.
        """
        try:
            action = self.actions[self.curIdx]
        except IndexError:
            print("Agent {} finished it's episode.".format(self.id))
            return None 
        if not "Finished" in action[1]:
            _, action = action[1].split("-")
            agent_pos = self.pos
            if action == "Left":
                new_pos = (agent_pos[0], agent_pos[1]-1)
            elif action == "Right":
                new_pos = (agent_pos[0], agent_pos[1]+1)
            elif action == "Up":
                new_pos = (agent_pos[0]-1, agent_pos[1])
            elif action == "Down":
                new_pos = (agent_pos[0]+1, agent_pos[1])
            elif action == "Interaction":
                new_pos = agent_pos
            else:
                raise AttributeError("Unknown action: {}".format(action))

            if self.environment.tiles[new_pos].passable:
                self.pos = new_pos

            if self.pos == agent_pos and ignore_duds:
                self.curIdx += 1
                return self.perform_action(ignore_duds)

            
                
        else: #Condition was finished
            return None
        
        self.curIdx += 1

        if callback is not None:
            if self.pos != agent_pos:
                self.stepCounter += 1
                callback(self.pos, self.stepCounter)

        #Setup new timer for next action
        if self.running and self.curIdx < len(self.actions):
            curTimestamp = self.actions[self.curIdx][0]
            delta = (curTimestamp - self.lastTimeStamp)/speedup
            self.lastTimeStamp = curTimestamp
            self.timer = threading.Timer(delta.total_seconds(), self.perform_action, [False, callback, speedup])
            self.timer.start()

        else:
            return self.pos
        

    def play(self, callback, speedup=1, start_step=None):
        self.stepCounter = 0
        self.reset()

        if start_step:
            # Move agent internally to start step before starting timer.
            while self.stepCounter < start_step:
                self.perform_action(ignore_duds=True)
                self.stepCounter += 1

        if self.curIdx > 0:
            delta = (self.actions[self.curIdx][0] - self.actions[self.curIdx-1][0])/speedup
        else:
            delta = (self.actions[self.curIdx][0] - self.lastTimeStamp)/speedup
        self.running = True
        self.lastTimeStamp = self.actions[self.curIdx][0]
        self.timer = threading.Timer(delta.total_seconds(), self.perform_action, [False, callback, speedup])
        self.timer.start()
        
    def pause(self):
        self.running = False
        self.timer.cancel()