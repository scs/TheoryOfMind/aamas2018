#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  9 15:11:45 2017

@author: jpoeppel
"""

import numpy as np
import os, re, ast
import csv

class EvaluationResult(object):
    """
        Container object for the results of a single trajectory.
    """

    def __init__(self, variant, participant_id, model_results):
        self.participant_id = participant_id
        self.variant = variant
        self.model_res = dict(model_results)
        self.timings = {}
        self.switches = []
        self.final_model = None

    @classmethod
    def from_row(cls, row, header):
        """
            Creates the result object from a row of the result files generated
            by the prepare_results_csv function in experiments.py.
        """
        model_res = {}
        for i, el in enumerate(header):
            if el == "Variant":
                var = row[i]
            elif el == "Participant ID":
                p_id = row[i]
            else:
                model_res[el] = float(row[i])
        return cls(var, p_id, model_res)

    def match(self, maze, condition, variant):
        """
            Checks this results maze, condition and variant with the parameters
            in order to determine if it matches those.
        """
        matchObj = re.match("condMap(\d{1,})_C(\d{1,})_V(\d{1,})", self.variant)
        if matchObj:
            map_n = matchObj.group(1)
            c_n = matchObj.group(2)
            v_n = matchObj.group(3)

        if maze != None and map_n != "{}".format(maze):
            return False
        if condition != None and c_n != "{}".format(condition):
            return False 
        if variant != None and v_n != "{}".format(variant):
            return False
        return True

    def get_variant_parts(self):
        """
            Will return the maze, condition and variant of this result.
        """
        matchObj = re.match("condMap(\d{1,})_C(\d{1,})_V(\d{1,})", self.variant)
        if matchObj:
            map_n = matchObj.group(1)
            c_n = matchObj.group(2)
            v_n = matchObj.group(3)
            return map_n, c_n, v_n

        return None, None, None

    def try_to_add_timing(self, row, model, header):
        """
            Will extract and parse the timing information from the row 
            (formatted) according to the prepare_timings_csv function in
            experiments.py. It will then add these timings to these results,
            if the participant_id and maze, condition and variant description
            matches.
        """
        
        for i, el in enumerate(header):
            if el == "Participant ID":
                p_id = row[i]
            elif el == "Variant":
                variant = row[i]
            else:
                # timings = ast.literal_eval(row[i])
                timings = row[i]
                timings = timings.strip("[]")
                timings = [float(el) for el in timings.split(",")]

        if model == "Full":
            model = "Full BToM"
            
        if self.variant == variant and self.participant_id == p_id:
            self.timings[model] = timings

    def try_to_add_switchings(self, row, header):
        """
            Will extract and parse the switching information from the row 
            (formatted) according to the prepare_switches_csv function in
            experiments.py. It will then add these switches to these results,
            if the participant_id and maze, condition and variant description
            matches.
        """
        
        for i, el in enumerate(header):
            if el == "Participant ID":
                p_id = row[i]
            elif el == "Variant":
                variant = row[i]
            elif el == "Final Results":
                final = row[i]
            else:
                switches = ast.literal_eval(row[i])
                # switches = row[i]
                # timings = timings.strip("[]")
                # timings = [tuple(el) for el in timings.split(",")]

        if self.variant == variant and self.participant_id == p_id:
            self.switches = switches
            self.final_model = final

class ResultContainer(object):
    """
        Container of all the results, which provides some quality of life
        functions to get specific subsets of the data.
    """

    def __init__(self):
        self.eval_results = []

    @classmethod
    def from_file(cls, path, surprise="KL", threshold="20"):
        """
            Fills the container from the 
            "results{surprise}_T{threshold}_Overall.csv" file under the
            given path, creating an EvaluationResult for each row within the 
            file.
        """
        res_container = cls()

        name = "results{}_T{}_Overall.csv".format(surprise, threshold)
        with open(path + os.path.sep + name, "r") as f:
            reader = csv.reader(f, delimiter=",")
            for i,row in enumerate(reader):
                if i == 0:
                    header = row 
                else:
                    res = EvaluationResult.from_row(row, header)
                    res_container.eval_results.append(res)
        return res_container


    def get_results(self, model, maze=None, condition=None, variant=None):
        """
            Collects the raw surprise measure results for the given model
            for all trajectories matching the given specifiers.
        """
        res = []
        # Collect only the results for runs matching the given specifiers
        for eval_res in [el for el in self.eval_results if el.match(maze, condition, variant)]:
            res.append(eval_res.model_res[model])
        return res

    def get_timings(self, model, maze=None, condition=None, variant=None):
        """
            Collects the raw timing measures for the given model
            for all trajectories matching the given specifiers.
        """
        res = []
        # Collect only the results for runs matching the given specifiers
        for eval_res in [el for el in self.eval_results if el.match(maze, condition, variant)]:
            res.extend([el*1000 for el in eval_res.timings[model]])
        return res

    def get_switches(self, maze=None, condition=None, variant=None):
        """
            Collects the raw switching information for the given model
            for all trajectories matching the given specifiers.
        """
        res = []
        # Collect only the results for runs matching the given specifiers
        for eval_res in [el for el in self.eval_results if el.match(maze, condition, variant)]:
            res.append(eval_res.switches)
        return res

    def get_available_variations(self):
        """
            Scans all contained EvaluationResults for the included mazes,
            conditions and variants and returns these three lists.
        """
        mazes = set([])
        conditions = set([])
        variants = set([])
        for eval_res in self.eval_results:
            m_n, c_n, v_n = eval_res.get_variant_parts()
            mazes.add(m_n)
            conditions.add(c_n)
            variants.add(v_n)
        return mazes, conditions, variants

    def _insert_timings(self, row, model, header):
        # print("Trying to add row: ", row)
        for eval_res in self.eval_results:
            eval_res.try_to_add_timing(row, model, header)
            
    def _insert_switches(self, row, header):
        # print("Trying to add row: ", row)
        for eval_res in self.eval_results:
            eval_res.try_to_add_switchings(row, header)

    def load_timings(self, path, models, surprise="KL", threshold="20"):
        """
            Function to read the "timings{surprise}_T{threshold}_{model}.csv"
            files in order to insert the contained timing information into
            the EvaluationResults.
        """
        for m in models:
            if m == "Full BToM":
                m = "Full"

            name = "timings{}_T{}_{}.csv".format(surprise, threshold, m)
            with open(path + os.path.sep + name, "r") as f:
                reader = csv.reader(f, delimiter=";")
                for i,row in enumerate(reader):
                    if i == 0:
                        header = row 
                    else:
                        self._insert_timings(row, m, header)

    def load_switches(self, path, models, surprise="KL", threshold="20"):
        """
            Function to read the "switches{surprise}_T{threshold}.csv"
            file in order to insert the contained switching information into
            the EvaluationResults.
        """

        name = "switches{}_T{}.csv".format(surprise, threshold)
        with open(path + os.path.sep + name, "r") as f:
            reader = csv.reader(f, delimiter=";")
            for i,row in enumerate(reader):
                if i == 0:
                    header = row 
                else:
                    self._insert_switches(row, header)
        
def compute_surprise_metrics_by_conditions(res_container, models, surprise, threshold):
    """
        Computes the mean and standard deviation of the surprise scores 
        for all given models over all trajectories and prepares the csv to be 
        used in the paper.
    """

    s = "model\toverall\toverallstd\ttwg\ttwgstd\ttw\ttwstd\ttg\ttgstd\n"
    for m in models:
        s += "{} Model".format(m)

        # Overall
        overall_res = res_container.get_results(m)
        s += "\t{}\t{}".format(np.mean(overall_res), np.std(overall_res)) 

        for c in ("1", "2", "3"):
            c_res = res_container.get_results(m, condition=c)
            s += "\t{}\t{}".format(np.mean(c_res), np.std(c_res))
        s += "\n"
    
    with open("overallAccuracy{}_T{}.csv".format(surprise, threshold), "w") as f:
        f.write(s)

def compute_surprise_metrics_by_variants(res_container, models, surprise, threshold):
    """
        Computes the mean and standard deviation of the surprise scores
        for all given models over all trajectories of all mazes and variants 
        separately and prepares the csv to be used in the paper.
    """

    mazes, _, variants = res_container.get_available_variations()

    for maze in mazes:
        for variant in variants:
            s = "model\toverall\toverallstd\ttwg\ttwgstd\ttw\ttwstd\ttg\ttgstd\n"
            for m in models:
                s += "{} Model".format(m)

                # Overall
                overall_res = res_container.get_results(m, maze=maze, variant=variant)
                s += "\t{}\t{}".format(np.mean(overall_res), np.std(overall_res)) 

                for c in ("1", "2", "3"):
                    c_res = res_container.get_results(m, maze=maze, condition=c, variant=variant)
                    s += "\t{}\t{}".format(np.mean(c_res), np.std(c_res))
                s += "\n"
            
            with open("accuracy{}_T{}_M{}V{}.csv".format(surprise,threshold, maze,variant), "w") as f:
                f.write(s)

def compute_timings(res_container, models, surprise, threshold):
    """
        Computes the mean, standard deviation and mean relative fraction of the
        timings taken by all models over all trajectories  and prepares the 
        csv to be used in the paper.
    """
    
    best_times = {k: 1000 for k in ("Overall", "1","2","3")}
    for m in models:
        overall_res = res_container.get_timings(m)
        best_times["Overall"] = min(best_times["Overall"], np.mean(overall_res))

        for c in ("1", "2", "3"):
            c_res = res_container.get_timings(m, condition=c)
            best_times[c] = min(best_times[c], np.mean(c_res))

    # Construct header    
    s = "Model\toverall\toverallstd\toverallRel\ttwg\ttwgstd\ttwgRel\ttw\ttwstd\ttwRel\ttg\ttgstd\ttgRel\n"
    for m in models:
        s += "{}".format(m)
        overall_res = res_container.get_timings(m)
        s += "\t{}\t{}\t{}".format(np.mean(overall_res), np.std(overall_res), np.mean(overall_res)/best_times["Overall"]) 

        for c in ("1", "2", "3"):
            c_res = res_container.get_timings(m, condition=c)
            s += "\t{}\t{}\t{}".format(np.mean(c_res), np.std(c_res), np.mean(c_res)/best_times[c])
        s += "\n"
    
    with open("timingsAggregated{}_T{}.csv".format(surprise, threshold), "w") as f:
        f.write(s)

def compute_timings_by_variants(res_container, models, surprise, threshold):
    """
        Computes the mean, standard deviation and mean relative fraction of the
        timings taken by all models over all trajectories of all mazes and variants
        separately and prepares the csv to be used in the paper.
    """

    mazes, _, variants = res_container.get_available_variations()

    for maze in mazes:
        for variant in variants:

            s = "model\toverall\toverallstd\ttwg\ttwgstd\ttw\ttwstd\ttg\ttgstd\n"
            for m in models:
                s += "{}".format(m)

                # Overall
                overall_res = res_container.get_timings(m, maze=maze, variant=variant)
                s += "\t{}\t{}".format(np.mean(overall_res), np.std(overall_res)) 

                for c in ("1", "2", "3"):
                    c_res = res_container.get_timings(m, maze=maze, condition=c, variant=variant)
                    s += "\t{}\t{}".format(np.mean(c_res), np.std(c_res))
                s += "\n"
            
            with open("timings{}_T{}_{}_{}.csv".format(surprise, threshold, maze,variant), "w") as f:
                f.write(s)

def _fill_dict(cond, row, _dict, _dict10, _dict20, _dictOther, _dictTotals):
    if cond in _dict:
        _dict[cond].append(int(row[1]))
        _dict10[cond].append(int(row[2]))
        _dict20[cond].append(int(row[3]))
        _dictOther[cond].append(int(row[5]))
        _dictTotals[cond] += int(row[6]) 
    else:
        _dict[cond] = [int(row[1])]
        _dict10[cond] = [int(row[2])]
        _dict20[cond] = [int(row[3])]
        _dictOther[cond] = [int(row[5])]
        _dictTotals[cond] = int(row[6]) 
    
    return _dict, _dict10, _dict20, _dictOther, _dictTotals

def _write_csv(name, _dict, _dict10, _dict20, _dictOther, _dictTotals):
    s = "condition\toptimal\toptimal10\toptimal20\tothers\n"
    for c_n in _dict.keys(): #("1", "2","3"): #optimality:
        s += "{}\t{}\t{}\t{}\t{}\n".format(c_n, sum(_dict[c_n])/_dictTotals[c_n], sum(_dict10[c_n])/_dictTotals[c_n],sum(_dict20[c_n])/_dictTotals[c_n], sum(_dictOther[c_n])/_dictTotals[c_n])
    
    with open("{}.csv".format(name), "w") as f:
        f.write(s)

def analyse_optimality(path, surprise="KL", threshold="22"):
    """
        Will read te file "numbers_optimal{surprise}_T{threshold}.csv" at the
        given path to compute the fractions of optimal and close to optimal
        behavior overall, as well as for all mazes and variants.
    """

    name = "numbers_optimal{}_T{}.csv".format(surprise, threshold)

    optimality = {}
    optimality10 = {}
    optimality20 = {}
    others = {}
    totals = {}
    optimality_map = {}
    optimality_map10 = {}
    optimality_map20 = {}
    others_map = {}
    totals_map = {}

    with open(path + os.path.sep + name, "r") as f:
        reader = csv.reader(f, delimiter=";")
        for i, row in enumerate(reader):
            if i == 0:
                continue
            variant = row[0]
            matchObj = re.match("condMap(\d{1,})_C(\d{1,})_V(\d{1,})", variant)
            if matchObj:
                map_n = matchObj.group(1)
                c_n = matchObj.group(2)
                v_n = matchObj.group(3)

            optimality, optimality10, optimality20, others, totals = _fill_dict(c_n, row, optimality, optimality10, optimality20, others, totals)

            map_var = map_n
            if not map_var in optimality_map:
                optimality_map[map_var] = {}
                optimality_map10[map_var] = {}
                optimality_map20[map_var] = {}
                others_map[map_var] = {}
                totals_map[map_var] = {}

            name = "{}_{}".format(c_n, v_n)

            optimality_map[map_var], optimality_map10[map_var], optimality_map20[map_var], others_map[map_var], totals_map[map_var] = _fill_dict(name, row, optimality_map[map_var], 
                                                                                                                optimality_map10[map_var], optimality_map20[map_var], others_map[map_var], totals_map[map_var])
    _write_csv("optimalitiesOverall", optimality, optimality10, optimality20, others, totals)    

    for map_var in optimality_map:
        _write_csv("optimalities_{}".format(map_var), optimality_map[map_var], optimality_map10[map_var], optimality_map20[map_var], others_map[map_var], totals_map[map_var])


def _computes_winnings(res_container, models, maze=None, condition=None, variant=None):
    winnings = {}
    for m1 in models:
        winnings[m1] = {}
        for m2 in models:
            if m1 != m2:
                res_m1 = res_container.get_results(m1, maze, condition, variant)
                res_m2 = res_container.get_results(m2, maze, condition, variant)
                counter_better = 0
                counter_worse = 0
                total = len(res_m1)
                worse = []
                worse_percent = []
                for i, score1 in enumerate(res_m1):
                    score2 = res_m2[i]
                    if score1 <= score2:
                        counter_better += 1
                    else:
                        counter_worse += 1
                        worse.append(score1-score2)
                        tmp = (score1-score2)/score2 * 100
                        if tmp < 0:
                            worse_percent.append(np.inf)
                        else:
                            worse_percent.append(tmp)

                winnings[m1][m2] = (counter_better/total, np.mean(worse), np.mean(worse_percent))                        

    return winnings

def prepate_winning_tables(res_container, models, surprise, threshold):
    """
        Computes the percentage and difference of the different models winning
        against each other across all trajectories as well as for the three
        conditions specifically.
    """

    winnings = {}
    for con in ("1","2","3"):
        winnings[con] = _computes_winnings(res_container, models, condition=con)

    winnings["overall"] = _computes_winnings(res_container, models)
    header = "Model\tFull BToM\tFull BToM Worse\tFull BToM WorseP\tTWG\tTWG Worse\tTWG WorseP\tTW\tTW Worse\tTW WorseP\tTG\tTG Worse\tTG WorseP\tSwitching\tSwitching Worse\tSwitching WorseP\n"
    for con in winnings:
        s = header
        for m1 in winnings[con]:
            s += "{}".format(m1)
            for m2 in ("Full BToM", "TWG", "TW", "TG", "Switching"): #winnings[con]:
                s += "\t"
                if m1 != m2:
                    s += "\t".join(["{:.6f}".format(el) for el in winnings[con][m1][m2]])
                else:
                    s += "nan\tnan\tnan"
            s += "\n"
        with open("winnings{}_T{}_{}.csv".format(surprise, threshold, con), "w") as f:
            f.write(s)


def prepare_switching_table(res_container, surprise, threshold):
    """
        Computes the average number of re-evaluations performed by the switching
        model for each maze, variant and condition and prepares a suitable csv
        to be used in the paper.
    """
    mazes, conditions, variants = res_container.get_available_variations()

    mean_switches = {}

    for m in mazes:
        mean_switches[m] = {}
        for c in conditions:
            mean_switches[m][c] = {}
            for var in variants:
                switches = res_container.get_switches(maze=m, condition=c, variant=var)
                mean_switches[m][c][var] = np.mean([len(el) for el in switches])

    # Construct header
    s = "Maze"
    for c in conditions:
        for v in variants:
            s += ";C{}V{}".format(c,v)
    s += "\n"

    for m in sorted(mean_switches.keys()):
        s += "{}".format(m)
        for c in mean_switches[m]:
            s += ";"
            s += ";".join(["{}".format(mean_switches[m][c][v]) for v in mean_switches[m][c]])
        s += "\n"

    with open("avg_switches{}_T{}.csv".format(surprise, threshold), "w") as f:
        f.write(s)

def compute_average_entropy(beliefs, name):
    """
        Function to compute the average entropy from the belief distributions
        over time and stores them in a csv suitable for plotting.
    """

    # Construct header
    s = "Step;" + ";".join("{}".format(m) for m in beliefs)
    s+= "\n"

    first_model = list(beliefs.keys())[0]

    for i in range(len(beliefs[first_model])):
        s += "{}".format(i)
        for m in beliefs:
            belief_container = beliefs[m][i]
            sum_entropies = 0
            for k, belief in belief_container.items():
                entropy = -1*sum([el*np.log(el) if el>0 else 0 for k,el in belief.items()])
                sum_entropies += entropy
            s += ";{}".format(sum_entropies/len(belief_container))
        s += "\n"

    with open("entropy" + name + ".csv", "w") as f:
        f.write(s)


if __name__ == "__main__":

    ## Path needs to be adapted to the folder where prepare_results_csv in
    ## experiments.py will write the results. Same for prepare_timings_csv
    path = "../rawData"

    ## Determine which surprise and threshold files should be used. These need
    ## to match what has been used in experiments.py as the files will be named
    ## accordingly.
    surprise = "KL"
    # surprise = "Rel"
    threshold = 22
    # threshold = "1.3"
    models = ("Full BToM","TWG","TW","TG","Switching")
    res_container = ResultContainer.from_file(path, surprise=surprise, threshold=threshold)
    res_container.load_switches(path, models, surprise=surprise, threshold=threshold)
    # prepare_switching_table(res_container, surprise, threshold)
    # compute_surprise_metrics_by_conditions(res_container, models, surprise, threshold)
    # compute_surprise_metrics_by_variants(res_container, models, surprise, threshold)
    # res_container.load_timings(path, models, surprise=surprise, threshold=threshold)
    # compute_timings(res_container, models, surprise, threshold)
    # compute_timings_by_variants(res_container, models, surprise, threshold)
    # prepate_winning_tables(res_container, models, surprise, threshold)


    ### For participant behavior without model inference
    prefix = path
    import experiments
    experiments.SURPRISE = surprise
    experiments.MODEL_THRESHOLD = threshold
    experiments.prepare_number_steps_csv("../Participant_data", prefix)
    experiments.prepare_completion_table("../Participant_data", prefix)
    experiments.get_optimal_runs("../Participant_data", prefix)
    analyse_optimality("../rawData")


    ### For the exmaples used in the paper
    # path = "../Participant_data/20/condMap2_C2_V1"
    # name = "P20_221_{}_{}".format(surprise, threshold)
    # path = "../Participant_data/29/condMap6_C3_V1"
    # name = "P29_631_{}_{}".format(surprise, threshold)
    # path = "../Participant_data/48/condMap3_C2_V1"
    # name = "P48_321_{}_{}".format(surprise, threshold)
    # beliefs = experiments.evaluate_single_experiment(path, name, prefix, write_beliefs=False)
    # compute_average_entropy(beliefs, name)