# Source Code for AAMAS 2018 publication

maintained by Jan Pöppel (jpoeppel@techfak.uni-bielefeld.de)

2016-2022

This repository should be considered a snapshot of the code used for the 
publication "Satisficing Models of Bayesian Theory of Mind for Explaining 
Behavior of Differently Uncertain Agents" by Jan Pöppel & Stefan Kopp 
at AAMAS 2018 (https://pub.uni-bielefeld.de/record/2917285).
This code is mostly identical to the code included in the supplementary data 
publication accompanying the mentioned paper, which can be found at https://doi.org/10.4119/unibi/2934879.
The two should only differ by this README and some minor comments.

The main file to generate the data is the `experiments.py` which sets up the different 
models and evaluates them on the given data. To this end, it expects to find a
folder containing the data called `Participant_data` in the parent directory of the directory that 
contains the script. 
This data is part of the aforementioned data publication and thus not added to this 
repository. 
Similarly, the data publication contains a `Conditions` folder that contains text descriptions of the 
different environments and conditions used in the paper, which some scripts/functions will also require in the 
same parent folder.

The script will assume that there exists a `rawData` folder alongside the `Participant_data` and `Conditions` 
folder to store its results, but you can also change this in the `__main__` block of the script.

The rest of the repository is structured as follows:

* `models/optimalModel.py` contains the implementations of the different (specialized) models discussed in the paper
* `blockworld.pyx` implements a basic 2D gridworld environment which is used for the experiments. It assumes that you use 
Cython to speed up the distance computations, but you can just rename the file to `blockworld.py` and run it without Cython 
as well. 
* `playback.py` is used to parse the behavioral data from `Participant_data` and replays the recorded behavior to be evaluated by 
the computational models 
* `analyze_results.py` contains scripts to parse the results from the experiments and computes the statistics used in the publication

## Hardware Dependencies

None

## Supported operating systems

Mac Os, Linux, Windows

## Build dependencies

None

## Run dependencies

See `requirements.txt` for a list of required dependencies (`Cython` is optional as long as you rename the `blockworld.pyx` file to 
`blockworld.py`) for a `pip` install.
You can use the following command to install all requirements using pip, ideally in a virtual environment:

```
pip install -r requirements.txt
```

Alternatively, you can use the `requirements_conda.txt` file to create a suitable conda environment with all required dependencies.
This can be created using:

```
conda create --name <env> --file requirements.txt
```

## Run instructions


By default, you just need to run the `experiments.py` script using

```
python experiments.py
```

This will evaluate all models discussed in the paper on the available data and generate result csv files (in a folder called 
`rawData` on the same level as `Participant_data` by default), including timing information. 
You can uncomment/change which models to use by providing the `rate_entire_episodes` function call in line #735 
with a model dictionary that flags (True/False) which models it should use. If not dictionary is specified (as currently)
the function will assume a dictionary that evaluates all available models:

```
 {"TWG": True,"TW": True,"TG":True,"Full BToM":True, "Switching": True}
 ```

After all result files have been created, you can use the 

```
python analyze_results.py
```

to compute the statistics used in the paper. You may want to adapt the thresholds and used surprise measure in both files 
if you experiment with different values.