#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  4 15:00:02 2017
Implementation of the approximately/beta-optimal models used in the 
AAMAS 2018 paper [TODO add doi/ref etc].
@author: jpoeppel
"""
from __future__ import division

import math
import copy

class OptimalModel(object):
    
    def __init__(self, surprise="KL", beta= 1.5, alpha=1, gamma=1):
        """
            Constructor for the models.

            Parameters
            ---------
            surprise: "KL"/"Rel" (default="KL")
                The surprise measurement (Kullback-Leibner or 
                Relative Difference) to be used.
            beta: float
                The optimality parameter for the action selection.
        """
        self.env = None
        self.beta = beta
        self.alpha = alpha
        self.gamma = gamma
        self.observe_probability = 1# 0.95
        # We normally only consider actions that change the position, so staying
        # at the current position is not a valid action, however, by setting
        # allow_stay, we will consider staying when normalizing the likelihood
        # which has the biggest effect on moving away from dead-ends, which so
        # far had probability of 1, even when moving away from a target, as there
        # were no alternatives
        self.allow_stay = False 
        
        # Change the called functions based on the chosen suprise measurement
        if surprise == "KL":
            # True World and Goal Belief model
            self.rate_episode = self.rate_episode_KL_score
            self.rate_action = self.rate_action_KL
            
            # True World belief model
            self.rate_episode_true_world = self.rate_episode_true_world_KL_score
            self.rate_action_true_world = self.rate_action_true_world_KL_score
            
            # True Goal belief model
            self.rate_episode_true_goal = self.rate_episode_true_goal_KL_score
            self.rate_action_true_goal = self.rate_action_true_goal_KL_score
            
            # No assumption model
            self.rate_episode_full = self.rate_episode_full_KL_score
            self.rate_action_full = self.rate_action_full_KL
            
        elif surprise == "Rel":
            #C1
            self.rate_episode = self.rate_episode_rel_score
            self.rate_action = self.rate_action_rel

            #C2        
            self.rate_episode_true_world = self.rate_episode_true_world_rel_score
            self.rate_action_true_world = self.rate_action_true_world_rel_score
            
            #C3
            self.rate_episode_true_goal = self.rate_episode_true_goal_rel_score
            self.rate_action_true_goal = self.rate_action_true_goal_rel_score
            
            #Full
            self.rate_episode_full = self.rate_episode_full_rel_score
            self.rate_action_full = self.rate_action_full_rel
            
        else:
            raise AttributeError("Invalid surprise value: {}".format(surprise))
        
        
    def set_environment(self, environment):
        self.env = environment
        self.env.visible_memory = set([])


#==============================================================================
#   Likelihood functions
#==============================================================================    

    def get_likelihood_optimal_beta_deterministic(self, first_state, 
                                                        second_state, goal_pos):
        r"""
            Likelihood function used in all models having a true World 
            belief. 

            Uses stochastic maximization similar to the BToM 
            model of Tenenbaum et. al:
                
            P(a|g) \prop exp(\beta Q)
            Where we use the distance as state values Q.
            
            Something to improve: 
            Only using the distance disregards the fact that some actions are
            going in the direction the agent came from, which should be a lot 
            less probable than just taking a slightly longer way.
            
            In the current setting, taking an action towards a longer way and
            turning back to where one came from are completely equal at this
            stage. 

            Parameters
            ----------
            first_state: tuple
                The position before taking the action.
            second_state: tuple
                The position after taking the action.
            goal_pos: tuple
                The position of the currently considered goal.

            Returns
            -------
                float
                The likelihood probability of going from first_state to 
                second_state, considering the target is goal_position.
        """
        
        norm = 0
        prob = 0

        beta = self.beta
        # m_dist = self.env.compute_distance(first_state, goal_pos)
        # m_dist2 = abs(first_state[0] - goal_pos[0]) + abs(first_state[1] - goal_pos[1])
        # m_dist = 0.6*m_dist+0.4*m_dist2
        # beta = -0.15 + 3.3*math.exp(-0.1*m_dist)
        for n in self.env.tiles[first_state].neighbours:
            if n.passable:
                d = self.env.compute_distance(n.pos, goal_pos)
                    
                tmp = math.exp(-beta*d)
                norm += tmp
                if n.pos == second_state:
                    prob = tmp
            if self.allow_stay:
                d = self.env.compute_distance(first_state, goal_pos)
                tmp = math.exp(-beta*d)
                norm += tmp
            
        return prob/norm
    
    def get_likelihood_optimal_freespace_beta(self, first_state, second_state, 
                                                                    goal_pos):
        """
            
            Likelihood function used in the models without true world belief.

            Similar to get_likelihood_optimal_beta_deterministic but computing
            the distance using the freespace assumption.

            This uses environment.compute_distance_partially_visible without
            remembering the observed elements since this will be done for
            all possible actions, only one of which is actually performed!

            Parameters
            ----------
            first_state: tuple
                The position before taking the action.
            second_state: tuple
                The position after taking the action.
            goal_pos: tuple
                The position of the currently considered goal.

            Returns
            -------
                float
                The likelihood probability of going from first_state to 
                second_state, considering the target is goal_position.
        """
        
        norm = 0
        prob = 0
        self.env._observe(first_state)
        for n in self.env.tiles[first_state].neighbours:
            if n.passable:
                d = self.env.compute_distance_partially_visible(n.pos, goal_pos, 
                                                                observe=False)

                tmp = math.exp(-self.beta*d)
                norm += tmp
                if n.pos == second_state:
                    prob = tmp
            
            if self.allow_stay:
                d = self.env.compute_distance(first_state, goal_pos)
                tmp = math.exp(-self.beta*d)
                norm += tmp
                    
        return prob/norm   
        
#==============================================================================
#   Goal belief update method
#==============================================================================    

    def _update_goal_beliefs(self, beliefs, goal_lookup, belief_lookup, 
                                                    first_state, second_state):
        r"""
            Updates the beliefs in one of 24 total possible worlds in which
            the 4 goals can be on any of the 4 possible locations, similar to 
            Baker and Tenenbaums BToM belief update:
            b_t(g_p) \prop P(o_t|s_t, g_p) * P(s_t|s_t-1, a_t-1, g_p) 
                                                                * b_t-1(g_p)
            
            The transition probability is currently deterministic thus can be
            omitted.
            
            P(o_t|s_t, g_p) represents the probability of observing o_t (in this
            2nd scenario the world with potentially some goal colors visible)
            when being in state 
            
            Parameters
            ----------
            beliefs: dict
                A dictionary of string world representations as keys and the
                corresponding current belief probability as value.
            goal_lookup: dict
                A dictionary containing goal symbols as keys and the 
                corresponding positions as values.
            belief_lookup: dict
                A dictionary of dictionaries mapping the belief representation
                to a map of goal symbol to beliefed goal position.
            first_state: tuple
                The position before performing the action.
            second_state: tuple
                The position after performing the action.
        """
        
        
        norm = 0
        obs_prob = 0
        goal_seen = False
        for bg in beliefs:
            for goal in goal_lookup:
                goal_pos  = belief_lookup[bg][goal]
                # If we see the currently assumed goal position
                if self.env.is_visible(goal_pos, second_state, radius=15):
                    goal_seen = True
                    # the probability of observing the actual goal there is ~1
                    # if bg represents a world where our goal is at that location
                    if goal_lookup[goal] == goal_pos:
                        obs_prob = self.observe_probability
                    # ~0 otherwise
                    else:
                        obs_prob = (1-self.observe_probability)
                
                
            if not goal_seen:
                obs_prob = self.observe_probability

                
            beliefs[bg] *= obs_prob
            norm += beliefs[bg]
            
        for bg in beliefs:
            beliefs[bg] /= norm


#==============================================================================
#   Rating methods for the True Goal and World Belief model
#==============================================================================    
    
    
    
    def rate_action_KL(self, last_state, new_state, goal_priors, goal_lookup):
        r"""
            Uses the Kullback-Leibler divergence to rate the surprise 
            of the performed action:

            S(a) = KL(Q(a)||P(a)) = \sum Q(a) log(\frac{Q(a)}{P(a)})

            with Q(a) being the probability of observing a, this simplifies to

            S(a) = -log(P(a)

            when assuming perfect observers. 

            Problem: If there are multiple optimal actions an optimal action 
            will still produce quite some surprise.
            It's debateable whether or not this should be intented. In some
            contexts this might be desired due to the inherent Ocam's Razor 
            effect this introduces.

            Parameters
            ----------
            first_state: tuple
                The position before performing the action.
            second_state: tuple
                The position after performing the action.
            goal_priors: dict
                A dictionary containing the prior probabilities for the known 
                goals
            goal_lookup: dict
                A dictionary mapping goal symbols to their positions.

            Returns
            -------
            score: float
                The total score for the performed action.
            goal_probs: dict
                A dictionary containing the goal probabilities after performing
                the action.
        """
        goal_probs = dict(goal_priors)
        pa = 0
        for goal in goal_probs:
            goal_pos = goal_lookup[goal]
            pa_g = self.get_likelihood_optimal_beta_deterministic(last_state, 
                                                            new_state, goal_pos)
            pa += pa_g * goal_probs[goal]
            goal_probs[goal] *= pa_g
            
        score = -math.log(pa) if pa > 0 else math.inf
        # Update/renormalize goal probabilities
        norm = sum(goal_probs.values())
        if norm == 0:
            goal_probs = {goal: 1/len(goal_probs) for goal in goal_probs}
        else:
            goal_probs = {goal: prior/norm for goal,prior in goal_probs.items()}
            
        return score, goal_probs
    
    
    def rate_action_rel(self, last_state, new_state, goal_priors, goal_lookup):
        r"""
            Uses the difference between the most likely and the performed action
            to rate the surprise of an action.

            S(a) = log(1 + P_{max} - P(a)) 

            where P_{max} is the probability of the most likely action.

            Parameters
            ----------
            first_state: tuple
                The position before performing the action.
            second_state: tuple
                The position after performing the action.
            goal_priors: dict
                A dictionary containing the prior probabilities for the known 
                goals
            goal_lookup: dict
                A dictionary mapping goal symbols to their positions.

            Returns
            -------
            score: float
                The total score for the performed action.
            goal_probs: dict
                A dictionary containing the goal probabilities after performing
                the action.
        """
        goal_probs = dict(goal_priors)

        tmp_goal_probs = copy.deepcopy(goal_probs)
        pa = 0
        p_max=0
        norm= 0
        for n in self.env.tiles[last_state].neighbours:
            if n.passable:
                tmp_a = 0
                
                for goal in goal_probs:
                    goal_pos = goal_lookup[goal]
                    # tmp_a_g = self.get_likelihood(last_state, n.pos, goalPos)
                    tmp_a_g = self.get_likelihood_optimal_beta_deterministic(
                                                    last_state, n.pos, goal_pos)
                    
                    tmp_a += tmp_a_g * tmp_goal_probs[goal]
                    # Only update pa and workingPriors for the action that 
                    # acutally happened
                    if n.pos == new_state:
                        pa = tmp_a 
                        # This here still punishes goals where there are
                        # multiple equally optimal actions since tmp_a_g
                        # will be a lot lower when there are mutliple 
                        # optimal actions
                        goal_probs[goal] *= tmp_a_g
                        
                p_max = max(p_max, tmp_a)
        
        
        score = math.log(1+p_max-pa)
        # Update/renormalize working priors
        norm = sum(goal_probs.values())
        if norm == 0:
            goal_probs = {goal: 1/len(goal_probs) for goal in goal_probs}
        else:
            goal_probs = {goal: prior/norm for goal,prior in goal_probs.items()}
        return score, goal_probs
    
    def rate_episode_KL_score(self, episode, goal_priors, goal_lookup):
        r"""
            Uses the Kullback-Leibler divergence to rate an entire episode:

            S(a_1,...,a_t) = \sum_{i=1}^t -log(P(a_i))
            with P(a_i) = \sum_{g \in G} P(a_i|g) * P(g)
            (see rate_action_KL for the derivation)

            Parameters
            ----------
            episode: list
                A list of visited states making up the current episode.
            goal_priors: dict
                A dictionary containing the prior probabilities for the known 
                goals
            goal_lookup: dict
                A dictionary mapping goal symbols to their positions.

            Returns
            -------
            score: float
                The total score for the episode
            goal_prob_list: list
                A list of dictionaries containing the probabilities of the goals
                after each taken action.
            score_list: list
                A list containing total score after each action in the episode.
        """
        
        goal_prob_list = []
        score_list = []
        goal_probs = dict(goal_priors)
        score = 0
        #Reset mpaa initialization
        self.env.mpaa_initialized = False
        for first, second in zip(episode, episode[1:]):
            
            goal_probs = dict(goal_probs)
            pa = 0
            norm = 0
            for goal in goal_probs:
                goal_pos = goal_lookup[goal]
                pa_g = self.get_likelihood_optimal_beta_deterministic(first, 
                                                            second, goal_pos) 
                pa += pa_g * goal_probs[goal]
                goal_probs[goal] *= pa_g
                
            score -= math.log(pa) if pa > 0 else -math.inf
            score_list.append(score)
            # Update/renormalize working priors
            norm = sum(goal_probs.values())
            if norm == 0:
                goal_probs = {goal: 1/len(goal_probs) for goal in goal_probs}
            else:
                goal_probs = {goal: prior/norm for goal,prior in goal_probs.items()}
            goal_prob_list.append(goal_probs)
            
        return score, goal_prob_list, score_list
    
    def rate_episode_rel_score(self, episode, goal_priors, goal_lookup):
        r"""
            Uses the relative difference between the optimal actions and the
            taken action to rate the suprise of an entire episode:

            S(a_1,...,a_t) = \sum_{i=1}^t log(1 + P_{max_i} - P(a_i))
            (see rate_action_rel for the derivation)


            Parameters
            ----------
            episode: list
                A list of visited states making up the current episode.
            goal_priors: dict
                A dictionary containing the prior probabilities for the known 
                goals
            goal_lookup: dict
                A dictionary mapping goal symbols to their positions.

            Returns
            -------
            score: float
                The total score for the episode
            goal_prob_list: list
                A list of dictionaries containing the priors after each taken
                action.
            score_list: list
                A list containing total score after each action in the episode.
        """
        
        goal_prob_list = []
        score_list = []
        goal_probs = dict(goal_priors)
        score = 0
        #Reset mpaa initialization
        self.env.mpaa_initialized = False
        i = 0
        for first, second in zip(episode, episode[1:]):
            
            goal_probs = dict(goal_probs)
            tmp_goal_probs = copy.deepcopy(goal_probs)
            pa = 0
            p_max=0
            norm= 0
            for n in self.env.tiles[first].neighbours:
                if n.passable:
                    tmp_a = 0
                    for goal in goal_probs:
                        goal_pos = goal_lookup[goal]
                        tmp_a_g = self.get_likelihood_optimal_beta_deterministic(first, 
                                                                n.pos, goal_pos)
                         
                        tmp_a += tmp_a_g * tmp_goal_probs[goal]
                        # Only update pa and goal_probs for the action that 
                        # acutally happened
                        if n.pos == second:
                            pa = tmp_a 
                            # This here still punishes goals where there are
                            # multiple equally optimal actions since tmp_a_g
                            # will be a lot lower when there are mutliple 
                            # optimal actions
                            goal_probs[goal] *= tmp_a_g
                            
                    p_max = max(p_max, tmp_a)
            
            i += 1
            score += math.log(1+p_max-pa)
            score_list.append(score)
            # Update/renormalize working priors
            norm = sum(goal_probs.values())
            if norm == 0:
                goal_probs = {goal: 1/len(goal_probs) for goal in goal_probs}
            else:
                goal_probs = {goal: prior/norm 
                                        for goal,prior in goal_probs.items()}
                
            goal_prob_list.append(goal_probs)
            
        return score, goal_prob_list, score_list


#==============================================================================
#   Rating functions for the True World belief model
#==============================================================================            
    
    def rate_action_true_world_KL_score(self, first_state, second_state, 
                                        goal_priors, goal_lookup, belief_priors, 
                                        belief_lookup):
        """
            Rates an action using the KL score (see rate_action_KL for more
            information on this). This method consideres different goal beliefs,
            i.e. it does not assume the true goal positions to be known. 

            In our world with 4 goals which can be at any of 4 specified 
            locations, we end up with 24 possible goal beliefs which need to be
            considered.

            Parameters
            ----------
            first_state: tuple
                The position before performing the action.
            second_state: tuple
                The position after performing the action.
            goal_priors: dict
                A dictionary containing the prior probabilities for the known 
                goals
            goal_lookup: dict
                A dictionary mapping goal symbols to their positions.
            belief_priors: dict
                Prior probabilities for the different goal beliefs.
            belief_lookup: dict
                A dictionary of dictionaries mapping goal beliefs to goal 
                positions.

            Returns
            -------
            score: float
                The total score for the performed action.
            goal_probs: dict
                A dictionary containing the goal probabilities after performing
                the action.
            beliefs: dict
                The updated belief distribution for the goal beliefs after 
                performing the action.
        """
        #Create copies of dictionaries to put them in lists for later use
        # print("Goal priors: ", goal_priors)
        goal_probs = dict(goal_priors)
        beliefs = dict(belief_priors)
        pa = 0
        for goal in goal_probs:
            pa_g = 0
            for b in beliefs:    
                goal_pos = belief_lookup[b][goal]
                pa_g_b = self.get_likelihood_optimal_beta_deterministic(first_state, 
                                                        second_state, goal_pos)
                pa_g += pa_g_b * beliefs[b] 
                
            pa += pa_g * goal_probs[goal]
            goal_probs[goal] *= pa_g
            
        score = -math.log(pa) if pa > 0 else math.inf
        self._update_goal_beliefs(beliefs, goal_lookup, belief_lookup, 
                                                    first_state, second_state)
        norm = sum(goal_probs.values())
        if norm == 0:
            goal_probs = {goal: 1/len(goal_probs) for goal in goal_probs}
        else:
            goal_probs = {goal: prior/norm for goal,prior in goal_probs.items()}
    
        return score, goal_probs, beliefs
    
    
    def rate_action_true_world_rel_score(self, first_sate, second_state, 
                                        goal_priors, goal_lookup, 
                                        belief_priors, belief_lookup):
        r"""
            Rates an action using the relative score (see rate_action_rel for 
            more information on this). This method consideres different goal 
            beliefs, i.e. it does not assume the true goal positions to be 
            known. 

            In our world with 4 goals which can be at any of 4 specified 
            locations, we end up with 24 possible goal beliefs which need to be
            considered.

            Parameters
            ----------
            first_state: tuple
                The position before performing the action.
            second_state: tuple
                The position after performing the action.
            goal_priors: dict
                A dictionary containing the prior probabilities for the known 
                goals
            goal_lookup: dict
                A dictionary mapping goal symbols to their positions.
            belief_priors: dict
                Prior probabilities for the different goal beliefs.
            belief_lookup: dict
                A dictionary of dictionaries mapping goal beliefs to goal 
                positions.

            Returns
            -------
            score: float
                The total score for the performed action.
            goal_probs: dict
                A dictionary containing the goal probabilities after performing
                the action.
            beliefs: dict
                The updated belief distribution for the goal beliefs after 
                performing the action.
        """
        #Create copies of dictionaries to put them in lists for later use
        goal_probs = dict(goal_priors)
   
        #Create copies of dictionaries to put them in lists for later use
        tmp_goal_probs = dict(goal_probs)
        beliefs = copy.deepcopy(belief_priors)
        tmp_beliefs = copy.deepcopy(beliefs)
        pa = 0
        p_max = 0
        for n in self.env.tiles[first_sate].neighbours:
            if n.passable:
                tmp_a = 0
                for goal in goal_probs:
                    tmp_a_g = 0
                    for b in beliefs:    
                        goal_pos = belief_lookup[b][goal]
                        tmp_a_g_b = self.get_likelihood_optimal_beta_deterministic(first_sate, 
                                                                n.pos, goal_pos)
                        tmp_a_g += tmp_a_g_b * tmp_beliefs[b] 
                        
                    tmp_a += tmp_a_g * tmp_goal_probs[goal]
                    if n.pos == second_state:
                        pa = tmp_a
                        goal_probs[goal] *= tmp_a_g
                    
                p_max = max(p_max, tmp_a)

        score = math.log(1+p_max-pa)
        self._update_goal_beliefs(beliefs, goal_lookup, belief_lookup, first_sate, second_state)
        norm = sum(goal_probs.values())
        if norm == 0:
            goal_probs = {goal: 1/len(goal_probs) for goal in goal_probs}
        else:
            goal_probs = {goal: prior/norm for goal,prior in goal_probs.items()}

        return score, goal_probs, beliefs

    def rate_episode_true_world_KL_score(self, episode, goal_priors, 
                                    goal_lookup, belief_priors, belief_lookup):
        """
            Uses the Kullback-Leibler divergence to rate an entire episode
            (see rate_episode_KL_score for details) while considering
            the 24 possible goal beliefs

            Parameters
            ----------
            episode: list
                A list of visited states making up the current episode.
            goal_priors: dict
                A dictionary containing the prior probabilities for the known 
                goals
            goal_lookup: dict
                A dictionary mapping goal symbols to their positions.
            belief_priors: dict
                Prior probabilities for the different goal beliefs.
            belief_lookup: dict
                A dictionary of dictionaries mapping goal beliefs to goal 
                positions.

            Returns
            -------
            score: float
                The total score for the episode
            (goal_prob_list, goal_belief_list): tuple
                A tuple containing both a list of dictionaries for the
                probabilities of the goals and the probabilities for the 
                different  goal beliefs after each taken action.
            score_list: list
                A list containing total score after each action in the episode.
        """
        goal_belief_list = []
        goal_prob_list = []
        score_list = []
        score = 0
        
        beliefs = dict(belief_priors)
        goal_probs = dict(goal_priors)
        
        for first, second in zip(episode, episode[1:]):
            
            #Create copies of dictionaries to put them in lists for later use
            goal_probs = dict(goal_probs)
            beliefs = dict(beliefs)

            pa = 0
            
            memory = {goal: {b: 0 for b in beliefs} for goal in goal_probs}
            for goal in goal_probs:
                pa_g = 0
                for b in beliefs:    
                    # The likelihood is dependent on both the goal and the belief
                    # about goal positions as both together allow us to compute the 
                    # utility of the resulting state
                    goal_pos = belief_lookup[b][goal]
                    pa_g_b = self.get_likelihood_optimal_beta_deterministic(first, 
                                                            second, goal_pos)
                    
                    pa_g += pa_g_b * beliefs[b] 
                    memory[goal][b] = pa_g_b
                
                        
                pa += pa_g * goal_probs[goal]
                
                goal_probs[goal] *= pa_g
                
            score -= math.log(pa) if pa > 0 else -math.inf
            score_list.append(score)

            #Update Beliefs:            
            self._update_goal_beliefs(beliefs, goal_lookup, belief_lookup, 
                                                                first, second)
            
            norm = sum(goal_probs.values())
            if norm == 0:
                goal_probs = {goal: 1/len(goal_probs) for goal in goal_probs}
            else:
                goal_probs = {goal: prior/norm for goal,prior in goal_probs.items()}
            goal_prob_list.append(goal_probs)
            goal_belief_list.append(beliefs)
            
        return score, (goal_prob_list, goal_belief_list), score_list
    
    def rate_episode_true_world_rel_score(self, episode, goal_priors, 
                                goal_lookup, belief_priors, belief_lookup):
        r"""
            Uses the relative difference between the optimal actions and the
            taken action to rate an entire episode
            (see rate_episode_rel_score for details) while considering
            the 24 possible goal beliefs

            Parameters
            ----------
            episode: list
                A list of visited states making up the current episode.
            goal_priors: dict
                A dictionary containing the prior probabilities for the known 
                goals
            goal_lookup: dict
                A dictionary mapping goal symbols to their positions.
            belief_priors: dict
                Prior probabilities for the different goal beliefs.
            belief_lookup: dict
                A dictionary of dictionaries mapping goal beliefs to goal 
                positions.

            Returns
            -------
            score: float
                The total score for the episode
            (goal_prob_list, goal_belief_list): tuple
                A tuple containing both a list of dictionaries for the
                probabilities of the goals and the probabilities for the 
                different  goal beliefs after each taken action.
            score_list: list
                A list containing total score after each action in the episode.
        """
        
        goal_belief_list = []
        goal_prob_list = []
        score_list = []
        score = 0
        
        beliefs = copy.deepcopy(belief_priors)
        goal_probs = dict(goal_priors)
        
        for first, second in zip(episode, episode[1:]):
            
            #Create copies of dictionaries to put them in lists for later use
            tmp_goal_probs = dict(goal_probs)
            beliefs = dict(beliefs)
            tmp_beliefs = dict(beliefs)
            goal_probs = dict(goal_probs)
            pa = 0
            p_max = 0
            for n in self.env.tiles[first].neighbours:
                if n.passable:
                    tmp_a = 0
                    for goal in goal_probs:
                        tmp_a_g = 0
                        for b in beliefs:
                            goal_pos = belief_lookup[b][goal]
                            tmp_a_g_b = self.get_likelihood_optimal_beta_deterministic(first, 
                                                                n.pos, goal_pos)
                            tmp_a_g += tmp_a_g_b * tmp_beliefs[b] 
                            
                        tmp_a += tmp_a_g * tmp_goal_probs[goal]    
                        if n.pos == second:
                            pa = tmp_a
                            goal_probs[goal] *= tmp_a_g
                        
                    p_max = max(p_max, tmp_a)
                        
            score += math.log(1+p_max-pa)
            score_list.append(score)
            
            # Update beliefs
            self._update_goal_beliefs(beliefs, goal_lookup, belief_lookup, 
                                                                first, second)
            
            norm = sum(goal_probs.values())
            if norm == 0:
                goal_probs = {goal: 1/len(goal_probs) for goal in goal_probs}
            else:
                goal_probs = {goal: prior/norm for goal,prior in goal_probs.items()}
            goal_prob_list.append(goal_probs)
            goal_belief_list.append(beliefs)
            
        return score, (goal_prob_list, goal_belief_list), score_list
    
    
#==============================================================================
#   Rating methods for the True Goal model
#==============================================================================    
    
    def rate_action_true_goal_KL_score(self, first_state, second_state, 
                                                    goal_priors, goal_lookup):
        r"""
            Rates an action using the KL score (see rate_action_KL for more
            information on this). This method uses the likelihood based on the
            free space assumption to deal with the differing world beliefs.

            Represents a belief over the different possible environments,
            meaning about (in the limit) all possible wall placements in the
            unknown areas:
                
            P(a|g) = \sum_b P(a|g,b) * P(b|g)
            
            
            P(a|g,b) is analoge to the likelihood_optimal, meaning 
            1-epsilon for actions getting closer to the goal g, and epsilon
            for other actions, but the optimality/distance is now computed 
            using the MPAA algorithm under the free-space assumption.
            
            Since in our 20*13 area, we already have 1.85e+78 different
            possible configurations of walls, we cannot consider all of those 
            naively. 
            
                            / 0, if b contains walls in unknown area 
                                -> free space assumption, we are not even 
                                considering beliefs with walls in unknown 
                                regions
            P(b|g) \prop  -  1/|w|* f, if b contains no walls in already 
                                seen areas where there is no wall. 
                                |w| represents the number of "forgotten" walls
                            \ 1- (1/|w| * f), if b corresponds to the already
                                observed environment without walls in the 
                                unknown area
                                
                                
                                    
            Assuming no forgetting (f=0) this simplifies to only 1 belief with
            probability 1 for the observed environment with free space 
            assumption.

            Parameters
            ----------
            first_state: tuple
                The position before performing the action.
            second_state: tuple
                The position after performing the action.
            goal_priors: dict
                A dictionary containing the prior probabilities for the known 
                goals
            goal_lookup: dict
                A dictionary mapping goal symbols to their positions.

            Returns
            -------
            score: float
                The total score for the performed action.
            goal_probs: dict
                A dictionary containing the goal probabilities after performing
                the action.
        """
        goal_probs = dict(goal_priors)
        pa = 0
        for goal in goal_probs:
            
            goal_pos = goal_lookup[goal]
            #Unfortunately, we need to reset the mpaa each time, since we
            #are considering different goals each time, we would ideally,
            #swap these two loops around, which we can do
            self.env.mpaa_initialized = False
            pa_g = self.get_likelihood_optimal_freespace_beta(first_state, 
                                                        second_state, goal_pos)
            pa += pa_g * goal_probs[goal]
            goal_probs[goal] *= pa_g
        
        score = -math.log(pa) if pa > 0 else math.inf
        # Update/renormalize working priors
        norm = sum(goal_probs.values())
        if norm == 0:
            goal_probs = {goal: 1/len(goal_probs) for goal in goal_probs}
        else:
            goal_probs = {goal: prior/norm for goal,prior in goal_probs.items()}

        return score, goal_probs   


    
    def rate_action_true_goal_rel_score(self, first_state, second_state, 
                                                goal_priors, goal_lookup):
        """
            Rates an action using the relative score (see rate_action_rel for 
            more information on this). This method uses the likelihood based on 
            the free space assumption to deal with the differing world beliefs.

            Parameters
            ----------
            first_state: tuple
                The position before performing the action.
            second_state: tuple
                The position after performing the action.
            goal_priors: dict
                A dictionary containing the prior probabilities for the known 
                goals
            goal_lookup: dict
                A dictionary mapping goal symbols to their positions.

            Returns
            -------
            score: float
                The total score for the performed action.
            goal_probs: dict
                A dictionary containing the goal probabilities after performing
                the action.
        """
        goal_probs = dict(goal_priors)
    
        tmp_goal_probs = dict(goal_probs)
        pa = 0
        p_max = 0
        for n in self.env.tiles[first_state].neighbours:
            if n.passable:
                tmp_a = 0
                for goal in goal_probs:
                    goal_pos = goal_lookup[goal]
                    # Unfortunately, we need to reset the mpaa each time, since 
                    # we are considering different goals each time, we would 
                    # ideally, swap these two loops around, which we can do.
                    self.env.mpaa_initialized = False
                    tmp_a_g = self.get_likelihood_optimal_freespace_beta(first_state, 
                                                                n.pos, goal_pos)
                    tmp_a += tmp_a_g * tmp_goal_probs[goal]
                    if n.pos == second_state:
                        goal_probs[goal] *= tmp_a_g
                        pa = tmp_a
                        
                p_max = max(p_max, tmp_a)
        
        score = math.log(1+p_max-pa)
        # Update/renormalize working priors
        norm = sum(goal_probs.values())
        if norm == 0:
            goal_probs = {goal: 1/len(goal_probs) for goal in goal_probs}
        else:
            goal_probs = {goal: prior/norm for goal,prior in goal_probs.items()}
            
        return score, goal_probs   

    def rate_episode_true_goal_KL_score(self, episode, goal_priors, goal_lookup):
        """
            Uses the Kullback-Leibler divergence to rate an entire episode
            (see rate_episode_KL_score for details) using the free space
            assumption.

            Parameters
            ----------
            episode: list
                A list of visited states making up the current episode.
            goal_priors: dict
                A dictionary containing the prior probabilities for the known 
                goals
            goal_lookup: dict
                A dictionary mapping goal symbols to their positions.

            Returns
            -------
            score: float
                The total score for the episode
            goal_prob_list: list
                A tuple containing both a list of dictionaries for the
                probabilities of the goals and the probabilities for the 
                different  goal beliefs after each taken action.
            score_list: list
                A list containing total score after each action in the episode.
        """
        
        goal_prob_list = []
        score_list = []
        goal_probs = dict(goal_priors)
        score = 0
        #Reset mpaa initialization
        self.env.mpaa_initialized = False
        #Reset visible memory
        self.env.visible_memory = set([])
        for first, second in zip(episode, episode[1:]):
            
            goal_probs = dict(goal_probs)
            pa = 0
            norm= 0
            for goal in goal_probs:
                #Unfortunately, we need to reset the mpaa each time, since we
                #are considering different goals each time, we would ideally,
                #swap these two loops around, which we can do
                self.env.mpaa_initialized = False
                goal_pos = goal_lookup[goal]
                pa_g = self.get_likelihood_optimal_freespace_beta(first, 
                                                            second, goal_pos)
                pa += pa_g * goal_probs[goal]
                
                goal_probs[goal] *= pa_g
                
            score -= math.log(pa) if pa > 0 else -math.inf
            score_list.append(score)
            
            # Update/renormalize working priors
            norm = sum(goal_probs.values())
            if norm == 0:
                goal_probs = {goal: 1/len(goal_probs) for goal in goal_probs}
            else:
                goal_probs = {goal: prior/norm for goal,prior in goal_probs.items()}
            goal_prob_list.append(goal_probs)

        if len(score_list) == 0:
            raise AttributeError("Scorelist empty: ", score_list)
        return score, goal_prob_list, score_list


            
    def rate_episode_true_goal_rel_score(self, episode, goal_priors, goal_lookup):
        """
            Uses the relative difference between the optimal actions and the
            taken action to rate an entire episode
            (see rate_episode_KL_score for details) using the free space
            assumption.

            Parameters
            ----------
            episode: list
                A list of visited states making up the current episode.
            goal_priors: dict
                A dictionary containing the prior probabilities for the known 
                goals
            goal_lookup: dict
                A dictionary mapping goal symbols to their positions.

            Returns
            -------
            score: float
                The total score for the episode
            goal_prob_list: list
                A tuple containing both a list of dictionaries for the
                probabilities of the goals and the probabilities for the 
                different goal beliefs after each taken action.
            score_list: list
                A list containing total score after each action in the episode.
        """
        
        goal_prob_list = []
        score_list = []
        goal_probs = dict(goal_priors)
        score = 0
        #Reset mpaa initialization
        self.env.mpaa_initialized = False
        #Reset visible memory
        self.env.visible_memory = set([])
        for first, second in zip(episode, episode[1:]):
            
            tmp_workingPriors = dict(goal_probs)
            goal_probs = dict(goal_probs)
            pa = 0
            p_max = 0
            for n in self.env.tiles[first].neighbours:
                if n.passable:
                    tmp_a = 0
                    for goal in goal_probs:
                        goal_pos = goal_lookup[goal]
                        # Unfortunately, we need to reset the mpaa each time, 
                        # since we are considering different goals each time, 
                        # we would ideally, swap these two loops around, 
                        # which we can do
                        self.env.mpaa_initialized = False
                        tmp_a_g = self.get_likelihood_optimal_freespace_beta(first, 
                                                                n.pos, goal_pos)
                        tmp_a += tmp_a_g * tmp_workingPriors[goal]
                        if n.pos == second:
                            goal_probs[goal] *= tmp_a_g
                            pa = tmp_a
                            
                    p_max = max(p_max, tmp_a)
                
            score += math.log(1+p_max-pa)
            score_list.append(score)
            # Update/renormalize working priors
            norm = sum(goal_probs.values())
            if norm == 0:
                goal_probs = {goal: 1/len(goal_probs) for goal in goal_probs}
            else:
                goal_probs = {goal: prior/norm for goal,prior in goal_probs.items()}
                
            goal_prob_list.append(goal_probs)
        
        return score, goal_prob_list, score_list

    
#==============================================================================
#   Rating methods for the Full BToM model
#==============================================================================    
   
    def rate_action_full_KL(self, first_state, second_state, goal_priors, 
                            goal_lookup, goal_belief_priors, belief_lookup, 
                            world_belief_priors):
        """
            Rates an action using the KL score (see rate_action_KL for more
            information on this). This method consideres both differing beliefs
            about goals but also about the world belief, considering both 
            the true belief of knowing the environment and the free space
            assumption.

            Parameters
            ----------
            first_state: tuple
                The position before performing the action.
            second_state: tuple
                The position after performing the action.
            goal_priors: dict
                A dictionary containing the prior probabilities for the known 
                goals
            goal_lookup: dict
                A dictionary mapping goal symbols to their positions.
            goal_belief_priors: dict
                Prior probabilities for the different goal beliefs.
            belief_lookup: dict
                A dictionary of dictionaries mapping goal beliefs to goal 
                positions.
            world_beliefs_priors: dict
                A dictionary containing the prior probabilities for true world
                beliefs and the free space assumption.

            Returns
            -------
            score: float
                The total score for the performed action.
            goal_probs: dict
                A dictionary containing the goal probabilities after performing
                the action.
            goal_beliefs: dict
                The updated belief distribution for the goal beliefes after 
                performing the action.
            world_beliefs: dict
                The updated belief distribution for the wall beliefs after 
                performing the action.
        """

        goal_beliefs = dict(goal_belief_priors)
        goal_probs = dict(goal_priors)
        tmp_goal_probs = dict(goal_probs)
        world_beliefs = dict(world_belief_priors)
        tmp_world_beliefs = dict(world_beliefs)
        tmp_a = 0

        memory = {goal: {b_g: {b_w: 0 for b_w in world_beliefs} 
                                for b_g in goal_beliefs} for goal in goal_probs}
        #Loop over goals P(g)
        for goal in goal_probs:
            tmp_a_g = 0
            #Loop over goal_beliefs P(b_g):
            for b_g in goal_beliefs:
                # The goal position is dependent on both the goal belief
                # and the goal desire!
                goal_pos = belief_lookup[b_g][goal]
                tmp_a_bg = 0
                #Loop over wall beliefs P(b_w):
                for b_w in world_beliefs:
                    # The likelihood is then dependent on the wall belief and
                    # the goal position
                    if b_w == "TrueBelief":
                        tmp_a_bg_bw = self.get_likelihood_optimal_beta_deterministic(first_state, 
                                                        second_state, goal_pos)
                    elif b_w == "FreeSpace":
                        #Reset mpaa initialization
                        self.env.mpaa_initialized = False
                        tmp_a_bg_bw = self.get_likelihood_optimal_freespace_beta(first_state, 
                                                        second_state, goal_pos)
                    else:
                        raise AttributeError("Wall belief {} not supported.".format(b_w))
                    
                    # Store likelihood to later compute marginals
                    memory[goal][b_g][b_w] = tmp_a_bg_bw
                    tmp_a_bg += tmp_a_bg_bw * tmp_world_beliefs[b_w]
                    
                tmp_a_g += tmp_a_bg * goal_beliefs[b_g]
                
            tmp_a += tmp_a_g * goal_probs[goal] #TODO better goal.id
            pa = tmp_a
            #Update goal priors
            goal_probs[goal] *= tmp_a_g
            #Update goal beliefs
            self._update_goal_beliefs(goal_beliefs, goal_lookup, belief_lookup, 
                                                    first_state, second_state)
            
        # Update wall beliefs:
        for b_w in world_beliefs:
            pa_bw = sum([memory[goal][b_g][b_w]*tmp_goal_probs[goal]*goal_beliefs[b_g] 
                                for goal in goal_probs for b_g in goal_beliefs])
            world_beliefs[b_w] *= pa_bw
            
        norm = sum(world_beliefs.values())
        world_beliefs = {b_w: val/norm if norm > 0 else 0 
                                        for b_w, val in world_beliefs.items()}
        score = -math.log(pa) if pa > 0 else math.inf
        #Normalise goal priors
        norm = sum(goal_probs.values())
        if norm == 0:
            goal_probs = {goal: 1/len(goal_probs) for goal in goal_probs}
        else:
            goal_probs = {goal: prior/norm for goal,prior in goal_probs.items()}
                
        return score, goal_probs, goal_beliefs, world_beliefs
            
    def rate_action_full_rel(self, first_state, second_state, goal_priors, 
                            goal_lookup, goal_belief_priors, belief_lookup, 
                            world_belief_priors):
        """
            Rates an action using the relative difference score 
            (see rate_action_rel for more information on this). This method 
            consideres both differing beliefs about goals but also about the 
            world belief, considering both the true belief of knowing the 
            environment and the free space assumption.

            Parameters
            ----------
            first_state: tuple
                The position before performing the action.
            second_state: tuple
                The position after performing the action.
            goal_priors: dict
                A dictionary containing the prior probabilities for the known 
                goals.
            goal_lookup: dict
                A dictionary mapping goal symbols to their positions.
            goal_belief_priors: dict
                Prior probabilities for the different goal beliefs.
            belief_lookup: dict
                A dictionary of dictionaries mapping goal beliefs to goal 
                positions.
            world_belief_priors: dict
                A dictionary containing the prior probabilities for true wall
                beliefs and the free space assumption.

            Returns
            -------
            score: float
                The total score for the performed action.
            goal_probs: dict
                A dictionary containing the goal probabilities after performing
                the action.
            goal_beliefs: dict
                The updated belief distribution for the goal beliefes after 
                performing the action.
            wall_beliefs: dict
                The updated belief distribution for the wall beliefs after 
                performing the action.
        """
        world_beliefs = dict(world_belief_priors)
        goal_beliefs = dict(goal_belief_priors)
        goal_probs = dict(goal_priors)
        tmp_world_beliefs = dict(world_beliefs)
        tmp_goal_probs = dict(goal_probs)
        tmp_goal_beliefs = dict(goal_beliefs)
        p_max = 0
        memory = {goal: {b_g: {b_w: 0 for b_w in world_beliefs} 
                                for b_g in goal_beliefs} for goal in goal_probs}
        #Loop over all possible "actions" for maximum
        for n in self.env.tiles[first_state].neighbours:
            if n.passable:
                tmp_a = 0
                #Loop over goals
                for goal in goal_probs:
                    tmp_a_g = 0
                    #Loop over goal_beliefs:
                    for b_g in goal_beliefs:
                        
                        # The goal position is dependent on both the goal belief
                        # and the goal desire!
                        goal_pos = belief_lookup[b_g][goal]
                        tmp_a_g_bg = 0
                        #Loop over wall beliefs:
                        for b_w in world_beliefs:
                            if b_w == "TrueBelief":
                                tmp_a_g_bg_bw = self.get_likelihood_optimal_beta_deterministic(first_state, 
                                                                n.pos, goal_pos)
                            elif b_w == "FreeSpace":
                                #Reset mpaa initialization
                                self.env.mpaa_initialized = False
                                tmp_a_g_bg_bw = self.get_likelihood_optimal_freespace_beta(first_state, 
                                                                n.pos, goal_pos)
                            else:
                                raise AttributeError("Wall belief {} not supported.".format(b_w))
                                
                            tmp_a_g_bg += tmp_a_g_bg_bw * tmp_world_beliefs[b_w]
                            
                            if n.pos == second_state:
                                # Store likelihood to later compute marginals
                                memory[goal][b_g][b_w] = tmp_a_g_bg_bw
                            
                        tmp_a_g += tmp_a_g_bg * tmp_goal_beliefs[b_g]
                        
                    tmp_a += tmp_a_g * tmp_goal_probs[goal] 
                    if n.pos == second_state:
                        pa = tmp_a
                        #Update goal priors
                        goal_probs[goal] *= tmp_a_g
                        #Update goal beliefs
                        self._update_goal_beliefs(goal_beliefs, goal_lookup, 
                                                belief_lookup, first_state, 
                                                second_state)
                        
                p_max = max(p_max, tmp_a)
                    
        # Update wall beliefs:
        for b_w in world_beliefs:
            pa_bw = sum([memory[goal][b_g][b_w]*tmp_goal_probs[goal]*goal_beliefs[b_g] 
                                for goal in goal_probs for b_g in goal_beliefs])
            world_beliefs[b_w] *= pa_bw
            
        norm = sum(world_beliefs.values())
        world_beliefs = {b_w: val/norm for b_w, val in world_beliefs.items()}
        score = math.log(1+p_max-pa)
        
        #Normalise goal priors
        norm = sum(goal_probs.values())
        if norm == 0:
            goal_probs = {goal: 1/len(goal_probs) for goal in goal_probs}
        else:
            goal_probs = {goal: prior/norm for goal,prior in goal_probs.items()}
            
        return score, goal_probs, goal_beliefs, world_beliefs
    
    def rate_episode_full_KL_score(self, episode, goal_priors, goal_lookup, 
                                    goal_belief_priors, belief_lookup,
                                    world_belief_priors):
        """
            Uses the KL score to rate an entire episode
            (see rate_episode_rel_score for details) while considering
            both differing goal and world beliefs.

            Parameters
            ----------
            episode: list
                A list of visited states making up the current episode.
            goal_priors: dict
                A dictionary containing the prior probabilities for the known 
                goals
            goal_lookup: dict
                A dictionary mapping goal symbols to their positions.
            goal_belief_priors: dict
                Prior probabilities for the different goal beliefs.
            belief_lookup: dict
                A dictionary of dictionaries mapping goal beliefs to goal 
                positions.
            world_beliefs_priors: dict
                A dictionary containing the prior probabilities for true world
                beliefs and the free space assumption.

            Returns
            -------
            score: float
                The total score for the episode
            (goal_prob_list, goal_belief_list, world_belief_list): tuple
                A tuple containing a list of dictionaries for the
                probabilities of the goals, the different goal beliefs and 
                the world beliefs after each taken action.
            score_list: list
                A list containing total score after each action in the episode.
        """
        
        goal_prob_list = []
        goal_belief_list = []
        world_belief_list = []
        score_list = []
        score = 0
        
        goal_beliefs = dict(goal_belief_priors)
        world_beliefs = dict(world_belief_priors)  # Represents P(b_w|b_g)
        goal_probs = dict(goal_priors)
        
        
        #Reset mpaa initialization
        self.env.mpaa_initialized = False
        #Reset visible memory
        self.env.visible_memory = set([])
        
        for first, second in zip(episode, episode[1:]):
            
            goal_beliefs = dict(goal_beliefs)
            goal_probs = dict(goal_probs)
            tmp_goal_probs = dict(goal_probs)
            world_beliefs = dict(world_beliefs)
            tmp_world_beliefs = dict(world_beliefs)
            tmp_a = 0
            
            memory = {goal: {b_g: {b_w: 0 for b_w in world_beliefs} 
                                for b_g in goal_beliefs} for goal in goal_probs}
            #Loop over goals P(g)
            for goal in goal_probs:
                tmp_a_g = 0
                #Loop over goal_beliefs P(b_g):
                for b_g in goal_beliefs:
                    # The goal position is dependent on both the goal belief
                    # and the goal desire!
                    goal_pos = belief_lookup[b_g][goal]
                    tmp_a_bg = 0
                    #Loop over wall beliefs P(b_w):
                    for b_w in world_beliefs:
                        # The likelihood is then dependent on the wall belief and
                        # the goal position
                        if b_w == "TrueBelief":
                            tmp_a_bg_bw = self.get_likelihood_optimal_beta_deterministic(first, 
                                                            second, goal_pos)
                        elif b_w == "FreeSpace":
                            #Reset mpaa initialization
                            self.env.mpaa_initialized = False
                            tmp_a_bg_bw = self.get_likelihood_optimal_freespace_beta(first, 
                                                                second, goal_pos)
                        else:
                            raise AttributeError("Wall belief {} not " \
                                                "supported.".format(b_w))
                        
                        # Store likelihood to later compute marginals
                        memory[goal][b_g][b_w] = tmp_a_bg_bw
                        tmp_a_bg += tmp_a_bg_bw * tmp_world_beliefs[b_w]
                        
                    tmp_a_g += tmp_a_bg * goal_beliefs[b_g]
                    
                tmp_a += tmp_a_g * goal_probs[goal] #TODO better goal.id
                pa = tmp_a
                #Update goal priors
                goal_probs[goal] *= tmp_a_g
                #Update goal beliefs
                self._update_goal_beliefs(goal_beliefs, goal_lookup, 
                                                belief_lookup, first, second)
                
            # Update wall beliefs:
            for b_w in world_beliefs:
                pa_bw = sum([memory[goal][b_g][b_w]*tmp_goal_probs[goal]*goal_beliefs[b_g] 
                                for goal in goal_probs for b_g in goal_beliefs])
                world_beliefs[b_w] *= pa_bw
                
            norm = sum(world_beliefs.values())
            world_beliefs = {b_w: val/norm for b_w, val in world_beliefs.items()}
            score -= math.log(pa)
            score_list.append(score)
            #Normalise goal priors
            norm = sum(goal_probs.values())
            if norm == 0:
                goal_probs = {goal: 1/len(goal_probs) for goal in goal_probs}
            else:
                goal_probs = {goal: prior/norm for goal,prior in goal_probs.items()}
                
            goal_prob_list.append(goal_probs)
            goal_belief_list.append(goal_beliefs)
            world_belief_list.append(world_beliefs)
                
        return score, (goal_prob_list, goal_belief_list, world_belief_list), score_list
    
    def rate_episode_full_rel_score(self, episode, goal_priors, goal_lookup, 
                                    goal_belief_priors, belief_lookup,
                                    world_belief_priors):
        """
            Uses the relative difference score to rate an entire episode
            (see rate_episode_rel_score for details) while considering
            both differing goal and world beliefs.

            Parameters
            ----------
            episode: list
                A list of visited states making up the current episode.
            goal_priors: dict
                A dictionary containing the prior probabilities for the known 
                goals
            goal_lookup: dict
                A dictionary mapping goal symbols to their positions.
            goal_belief_priors: dict
                Prior probabilities for the different goal beliefs.
            belief_lookup: dict
                A dictionary of dictionaries mapping goal beliefs to goal 
                positions.
            world_beliefs_priors: dict
                A dictionary containing the prior probabilities for true world
                beliefs and the free space assumption.

            Returns
            -------
            score: float
                The total score for the episode
            (goal_prob_list, goal_belief_list, world_belief_list): tuple
                A tuple containing a list of dictionaries for the
                probabilities of the goals, the different goal beliefs and 
                the world beliefs after each taken action.
            score_list: list
                A list containing total score after each action in the episode.
        """
        goal_prob_list = []
        goal_belief_list = []
        world_belief_list = []
        score_list = []
        score = 0
        goal_beliefs = copy.deepcopy(goal_belief_priors)
        wall_beliefs = copy.deepcopy(world_belief_priors)
        goal_probs = copy.deepcopy(goal_priors)
        #Reset mpaa initialization
        self.env.mpaa_initialized = False
        #Reset visible memory
        self.env.visible_memory = set([])
        for first, second in zip(episode, episode[1:]):
            goal_probs = dict(goal_probs)
            tmp_world_beliefs = dict(wall_beliefs)
            tmp_goal_probs = dict(goal_probs)
            tmp_goal_beliefs = dict(goal_beliefs)
            p_max = 0
            memory = {goal: {b_g: {b_w: 0 for b_w in wall_beliefs} 
                            for b_g in goal_beliefs} for goal in goal_probs}
            #Loop over all possible "actions" for maximum
            for n in self.env.tiles[first].neighbours:
                if n.passable:
                    tmp_a = 0
                    #Loop over goals
                    for goal in goal_probs:
                        tmp_a_g = 0
                        #Loop over goal_beliefs:
                        for b_g in goal_beliefs:
                            # The goal position is dependent on both the goal 
                            # belief and the goal desire!
                            goal_pos = belief_lookup[b_g][goal]
                            tmp_a_g_bg = 0
                            #Loop over wall beliefs:
                            for b_w in wall_beliefs:
                                if b_w == "TrueBelief":
                                    tmp_a_g_bg_bw = self.get_likelihood_optimal_beta_deterministic(first, 
                                                                n.pos, goal_pos)
                                elif b_w == "FreeSpace":
                                    #Reset mpaa initialization
                                    self.env.mpaa_initialized = False
                                    tmp_a_g_bg_bw = self.get_likelihood_optimal_freespace_beta(first, 
                                                                n.pos, goal_pos)
                                    
                                else:
                                    raise AttributeError("Wall belief {} not " \
                                                    "supported.".format(b_w))
                                    
                                tmp_a_g_bg += tmp_a_g_bg_bw * tmp_world_beliefs[b_w]
                                
                                if n.pos == second:
                                    # Store likelihood to later compute 
                                    # marginals
                                    memory[goal][b_g][b_w] = tmp_a_g_bg_bw
                                
                            tmp_a_g += tmp_a_g_bg * tmp_goal_beliefs[b_g]
                            
                        tmp_a += tmp_a_g * tmp_goal_probs[goal] 
                        if n.pos == second:
                            pa = tmp_a
                            #Update goal priors
                            goal_probs[goal] *= tmp_a_g
                            #Update goal beliefs
                            self._update_goal_beliefs(goal_beliefs, goal_lookup, 
                                                belief_lookup, first, second)
                            
                    p_max = max(p_max, tmp_a)
                        
            # Update wall beliefs:
            for b_w in wall_beliefs:
                pa_bw = sum([memory[goal][b_g][b_w]*tmp_goal_probs[goal]*goal_beliefs[b_g] 
                                for goal in goal_probs for b_g in goal_beliefs])
                wall_beliefs[b_w] *= pa_bw
                
            norm = sum(wall_beliefs.values())
            wall_beliefs = {b_w: val/norm for b_w, val in wall_beliefs.items()}
            score += math.log(1+p_max-pa)
            score_list.append(score)
            
            #Normalise goal priors
            norm = sum(goal_probs.values())
            if norm == 0:
                goal_probs = {goal: 1/len(goal_probs) for goal in goal_probs}
            else:
                goal_probs = {goal: prior/norm for goal,prior in goal_probs.items()}
                
            goal_prob_list.append(goal_probs)
            goal_belief_list.append(goal_beliefs)
            world_belief_list.append(wall_beliefs)
                
        return score, (goal_prob_list, goal_belief_list, world_belief_list), score_list