#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script to evaluate the recorded data described in 
Pöppel, Kopp, "Satisficing Models of Bayesian Theory of Mind for Explaining 
Behavior of Differently Uncertain Agents" published in the AAMAS 2018 
proceedings, using the models from models/optimalModel.py and analyse the 
results.
This script assumes that it can find a folder called "Participant_data" 
in the parent folder of the folder containing this script. You can find the "Participant_data" 
as well as this code (with an earlier introduction) in the data
publication: https://doi.org/10.4119/unibi/2934879.

For its results, the script currently assumes that it can find a folder called "rawData" 
in the same parent folder, but you can change this prefix below.

@author: jpoeppel
"""

try:
    # Compiles .pyx files (currently only blockworld) using cython, even without
    # specifying any types, this already gives a decent speedup
    # Remember that you would need to rename blockworld.py to blockworld.pyx
    # for this to have any effect!
    import pyximport; pyximport.install()
except:
    # We do not need to compile those if pyximport is not installed
    # In this case, Python will however not find the blockworld module if it
    # was renamed to blockworld.pyx!
    pass

import itertools, time, copy, re, os

import playback
import models

# Optimality factor for the stochastic maximization.
BETA = 1.5

# The used suprise measure, implemented are the Kullback-Leibler divergence 
# ("KL") based surprised and the relative difference surprise ("Rel")
SURPRISE = "KL"
# SURPRISE = "Rel"

# Threshold for the switching model 

if SURPRISE == "KL":
    MODEL_THRESHOLD = 22 #For KL score
else:
    MODEL_THRESHOLD = 1.3 #For rel score

USE_CACHING = True


def rate_single_episode(env, targets, agent, true_goal, methods=None, params=None):
    if not methods:
        methods = {"TWG":True, "TW": True, "TG": True, "Full BToM": True}
    model = models.OptimalModel(surprise=SURPRISE, beta=BETA)
    goal_priors = {info["symbol"]: 1/len(targets) 
                                    for info in targets.values()}
    goal_lookup = {info["symbol"]: pos for pos, info in targets.items()}
    perms = list(itertools.permutations([info["symbol"] for info in targets.values()]))
    num_perms = len(perms)
    
    pos = list(targets.keys())
    goal_belief_priors = {perm: 1/num_perms for perm in perms}
    world_belief_priors = {method: 0.5 
                            for method in ["TrueBelief", "FreeSpace"]} 

    goal_belief_priors_unknown = {method: 0.5 
                            for method in ["Known", "Unknown"]}                     

    belief_lookup = {perm: {info["symbol"]: pos[perm.index(info["symbol"])] 
                        for info in targets.values()} for perm in perms}
    true_belief = {info["symbol"]: pos for pos, info in targets.items()}

    for perm in belief_lookup:
        if belief_lookup[perm] == true_belief:
            true_perm = perm
    model.set_environment(env)
    episode = [agent.pos]
    episode_done = False
    while not episode_done:
        new_pos = agent.perform_action(ignore_duds=True)
        if new_pos is None:
            episode_done = True
        else:
            episode.append(new_pos)
        
    results = {}
    if methods.get("TWG", False):
        # True World and Goal Belief model
        m1 = model.rate_episode(episode, goal_priors, goal_lookup)
        results["TWG"] = m1
    if methods.get("TW", False):
        # True World model
        m2 = model.rate_episode_true_world(episode, goal_priors, 
                            goal_lookup, goal_belief_priors, belief_lookup)
        results["TW"] = m2
    if methods.get("TG", False):
        # True Goal model
        m3 = model.rate_episode_true_goal(episode, goal_priors, goal_lookup)
        results["TG"] = m3
    if methods.get("Full BToM", False):
        # No Assumption model
        m4 = model.rate_episode_full(episode, goal_priors, goal_lookup, 
                            goal_belief_priors, belief_lookup,
                            world_belief_priors)
        results["Full BToM"] = m4

    return results, true_perm #(m1, m2, m3, m4)

def rate_entire_episodes(path, models=None):
    """
        Will evaluate all recordings found at the given path and evaluate the
        entire episodes with each of the 4 models.

        Parameters
        ----------
        path: str
            The path to the participant recordings.

        Returns
        -------
            dict of dicts
            A dictionary containing the results for each condition for each
            model, first seperated by models, then by condition.
    """

    if models is None:
        models = {"TWG": True,"TW": True,"TG":True,"Full BToM":True, "Switching": True}
        # models = {"Switching": True}
    variants = playback.crawl_results(path, use_caching=USE_CACHING)
    counter = 0
 
    results_container = {}
    t0 = time.time()
    for i, variant in enumerate(variants):
        print("Working on variant {}(#{})".format(variant, i))
        # Prepare container
        for m in models:
            if not m in results_container:
                results_container[m] = {}
            results_container[m][variant] = {} #[]

        for env, targets, agent, true_goal in variants[variant]:
            counter += 1
            results, true_perm = rate_single_episode(env, targets, agent, true_goal, methods=models)
            for m in results:
                # results_container[m][condition].append((results[m], participant_id))
                results_container[m][variant][agent.id] = results[m]

            # Reset agent here, it will benefit from potentially cached 
            # distances in the environment from the earlier models though
            agent.reset()
            if models.get("Switching", False):
                results_switching = rate_single_episode_switching(env, targets, agent, true_goal)
                # cur_surprise, (timings, model_switches, prior_list), score_list = rate_single_episode_switching(env, targets, agent, true_goal)
                results_container["Switching"][variant][agent.id] = results_switching

    print("Evaluation took: ", time.time()-t0)
    print("handled {} runs".format(counter))
    return results_container 
    

def rate_single_episode_switching(env, targets, agent, true_goal):
    agent.reset()
    model = models.OptimalModel(surprise=SURPRISE, beta=BETA)
    goal_priors = {info["symbol"]: 1/len(targets) 
                                    for info in targets.values()}
    goal_lookup = {info["symbol"]: pos for pos, info in targets.items()}
    perms = list(itertools.permutations([info["symbol"] 
                                        for info in targets.values()]))
    num_perms = len(perms)
    
    pos = list(targets.keys())
    goal_belief_priors = {perm: 1/num_perms for perm in perms}
    belief_lookup = {perm: {info["symbol"]: pos[perm.index(info["symbol"])] 
                        for info in targets.values()} for perm in perms}

    true_belief = {info["symbol"]: pos for pos, info in targets.items()}
    for perm in belief_lookup:
        if belief_lookup[perm] == true_belief:
            true_perm = perm


    model.set_environment(env)
    episode = [agent.pos]
    episode_done = False

    world_belief_priors = {"FreeSpace": 0.5, "TrueBelief": 0.5}

    true_goal_belief = dict(goal_belief_priors)
    for b in true_goal_belief:
        if b == true_perm:
            true_goal_belief[b] = 1
        else:
            true_goal_belief[b] = 0
    
    # Start with the True World and Goal Belief (the simples) model
    cur_model = "TWG"

    cur_intention_belief = dict(goal_priors)
    cur_goal_belief = dict(goal_belief_priors)

    cur_goal_belief = true_goal_belief
    true_world_belief = {"FreeSpace": 0, "TrueBelief": 1}
    cur_world_belief = true_world_belief

    belief_priors = {}

    model_switches = []
    timings = []
    score_list = []
    prior_list = []
    cur_surprise = 0
    # Initialize model threshold
    model_threshold = MODEL_THRESHOLD
    while not episode_done:
        old_pos = agent.pos
        new_pos = agent.perform_action(ignore_duds=True)
        if new_pos is None:
            episode_done = True

        else:
            episode.append(new_pos)

            t0= time.time()
            # Use only the currently active model!
            if cur_model == "TWG":
                score, cur_intention_belief = model.rate_action(old_pos, new_pos, 
                                    cur_intention_belief, goal_lookup)
                    
            elif cur_model == "TW":
                score, cur_intention_belief, cur_goal_belief = model.rate_action_true_world(old_pos,
                                        new_pos, cur_intention_belief, 
                                        goal_lookup, cur_goal_belief, 
                                        belief_lookup)
                    
            elif cur_model == "TG":
                score, cur_intention_belief = model.rate_action_true_goal(old_pos,
                                        new_pos, cur_intention_belief, 
                                        goal_lookup)
                # print("Cur_intention_belief (TG case): ", cur_intention_belief)
            
            timing = time.time()-t0
            cur_surprise += score

            # Check if we need to switch the model
            if cur_surprise >= model_threshold:
                if cur_model == "TWG":
                    score_m1 = cur_surprise
                    intention_b_m1 = cur_intention_belief
                    
                    score_m2, beliefs_m2, _ = model.rate_episode_true_world(episode,
                                    goal_priors, goal_lookup, 
                                    goal_belief_priors, belief_lookup)
                    intention_b_m2 = beliefs_m2[0][-1]
                    goal_beliefs_m2 = beliefs_m2[1][-1]

                    score_m3, beliefs_m3, _ = model.rate_episode_true_goal(episode,
                                    goal_priors, goal_lookup)
                    # print("Beliefs m3 (TWG case): ", beliefs_m3[-1])
                    intention_b_m3 = beliefs_m3[-1]
                    # world_belief_m3 = beliefs_m3[1][-1]
                    
                elif cur_model == "TW":
                    score_m1, beliefs_m1, _ = model.rate_episode(episode, 
                                    goal_priors, goal_lookup)
                    intention_b_m1 = beliefs_m1[-1]
                    score_m2 = cur_surprise
                    intention_b_m2 = cur_intention_belief
                    goal_beliefs_m2 = cur_goal_belief
                    
                    score_m3, beliefs_m3, _ = model.rate_episode_true_goal(episode,
                                    goal_priors, goal_lookup)
                    # print("Beliefs m3: ", beliefs_m3[-1])
                    intention_b_m3 = beliefs_m3[-1]
                    # world_belief_m3 = beliefs_m3[1][-1]
                elif cur_model == "TG":
                    score_m1, beliefs_m1, _ = model.rate_episode(episode, 
                                    goal_priors, goal_lookup)
                    intention_b_m1 = beliefs_m1[-1]
                    
                    score_m2, beliefs_m2, _ = model.rate_episode_true_world(episode,
                                    goal_priors, goal_lookup, 
                                    goal_belief_priors, belief_lookup)
                    intention_b_m2 = beliefs_m2[0][-1]
                    goal_beliefs_m2 = beliefs_m2[1][-1]
                    
                    score_m3 = cur_surprise
                    intention_b_m3 = cur_intention_belief
                    # world_belief_m3 = cur_world_belief
                
                if score_m2 < score_m1 and score_m2 < score_m3:
                    new_model = "TW"
                    cur_surprise = score_m2
                    cur_intention_belief = intention_b_m2
                    cur_goal_belief = goal_beliefs_m2
                    # cur_world_belief = true_world_belief
                    
                elif score_m3 < score_m1 and score_m3 < score_m2:
                    new_model = "TG"
                    cur_surprise = score_m3
                    cur_intention_belief = intention_b_m3
                    cur_goal_belief = true_goal_belief
                    # cur_world_belief = world_belief_m3
                else:
                    new_model = "TWG"
                    cur_surprise = score_m1
                    cur_intention_belief = intention_b_m1
                    cur_goal_belief = true_goal_belief
                    # cur_world_belief = true_world_belief

                    
                #Increase threshold after switching
                model_threshold *= 1.5
                model_switches.append((cur_model, new_model, 
                            len(episode), score_m1, score_m2, score_m3))
                cur_model = new_model
            switchtime = time.time()-t0


            belief_priors["desire"] = cur_intention_belief
            belief_priors["goal"] = cur_goal_belief
            belief_priors["world"] = {"FreeSpace": 1 if cur_model == "TG" else 0, "TrueBelief": 0 if cur_model == "TG" else 1}

            # timings.append(timing+switchtime)
            timings.append(switchtime)
            score_list.append(cur_surprise)
            prior_list.append(copy.deepcopy(belief_priors))

    return cur_surprise, (list(timings), model_switches, prior_list), score_list

def get_timings(path, model_name):
    """
        Function to time the different models.

        Parameters
        ----------
        path: str
            The path to the participant recordings.

        model_name: str
            The model which is to be timed. Suitable models are
            TWG = True World and Goal Belief
            TW = True World Belief
            TG = True Goal Belief
            NA = No Assumption/Full model

        Returns
        -------
            dict of dicts
            A dictionary containing the timings for all variants for the
            specified model.
    """

    variants = playback.crawl_results(path, use_caching=USE_CACHING)
    model = models.OptimalModel(surprise=SURPRISE, beta=BETA)
    counter = 0
    
    results = {}
    for i, variant in enumerate(variants):
        results[variant] = []
        for env, targets, agent, true_goal in variants[variant]:
            counter += 1
            goal_priors = {info["symbol"]: 1/len(targets) 
                                    for info in targets.values()}
            goal_lookup = {info["symbol"]: pos for pos, info in targets.items()}
            goal_belief_priors = {info["symbol"]: {pos: 1/len(targets) 
                                    for pos in targets.keys()} 
                                    for info in targets.values()}
            perms = list(itertools.permutations([info["symbol"] 
                                                for info in targets.values()]))
            num_perms = len(perms)
            
            pos = list(targets.keys())
            goal_belief_priors = {perm: 1/num_perms for perm in perms}
            world_belief_priors = {method: 0.5 
                                    for method in ["TrueBelief", "FreeSpace"]} 
            belief_lookup = {perm: {info["symbol"]: pos[perm.index(info["symbol"])] 
                                for info in targets.values()} for perm in perms}
            
            model.set_environment(env)
            episode = [agent.pos]
            episode_done = False
            
            cur_intention_belief = dict(goal_priors)
            cur_goal_belief = dict(goal_belief_priors)
            cur_world_belief = dict(world_belief_priors)

            timings = []
            while not episode_done:
                old_pos = agent.pos
                new_pos = agent.perform_action(ignore_duds=True)
                if new_pos is None:
                    episode_done = True

                else:
                    episode.append(new_pos)

                    t0= time.time()
                    # Use only the currently active model!
                    if model_name == "TWG":
                        _, cur_intention_belief = model.rate_action(old_pos, new_pos, 
                                            cur_intention_belief, goal_lookup)
                            
                    elif model_name == "TW":
                        _, cur_intention_belief, cur_goal_belief = model.rate_action_true_world(old_pos,
                                                new_pos, cur_intention_belief, 
                                                goal_lookup, cur_goal_belief, 
                                                belief_lookup)
                            
                    elif model_name == "TG":
                        _, cur_intention_belief = model.rate_action_true_goal(old_pos,
                                                new_pos, cur_intention_belief, 
                                                goal_lookup)
                    elif model_name == "NA":
                        _, cur_intention_belief, cur_goal_belief, cur_world_belief = model.rate_action_full(old_pos,
                                            new_pos, cur_intention_belief, 
                                            goal_lookup, cur_goal_belief, 
                                            belief_lookup, cur_world_belief)
                    else:
                        raise ValueError("Unknown model: ", model_name)
                    
                    timing = time.time()-t0
                    timings.append(timing)
            
            results[variant].append((agent.id, list(timings)))
    return results

def prepare_results_csv(prefix, results):
    """
        Prepares the rating results of the different models and conditions into
        structured csv-files, which could then be statistically anaylsed,
        e.g. via http://tec.citius.usc.es/stac/index.html.

        Will create one file for each of the conditions "No Uncertainty" (C1),
        "Goal Uncertainty" (C2) and "World Uncertainty" (C3) as well as a file
        covering all conditions ("Overall").

        Parameters
        ----------
        prefix: str
            Basepath for where to store the csv files. 
        results: dict
            A dictionary containing the results for the different models to be
            considered (Structure as returned by "rate_entire_episodes")
    """
    # Conditions where initially named 1,2,3, so we want to define the name 
    # mapping here
    CONDITION_MAP = {"NU": "C1", "DU": "C2", "PU": "C3", "Overall":"Overall"}
    conditions = {"NU": [], "DU": [], "PU": [], "Overall": []}
    first_model = list(results.keys())[0]

    for variant in results[first_model]:
        for p_id in results[first_model][variant]:
            for con in conditions:
                if CONDITION_MAP[con] in variant:
                    tmp = {m: results[m][variant][p_id][0] for m in results.keys()}
                    tmp["variant"] = variant
                    tmp["part"] = p_id
                    conditions[con].append(dict(tmp))
    
    # Collect "overall"
    for key in conditions:
        if key != "Overall":
            conditions["Overall"].extend(conditions[key])
    for condition in conditions:
        with open(prefix+os.path.sep+ "results{}_T{}_{}.csv".format(SURPRISE, MODEL_THRESHOLD,condition), "w") as f:
            f.write("{},Variant,Participant ID\n".format(",".join(results.keys())))
            for run_nr, run in enumerate(conditions[condition]):
                f.write(",".join(["{}".format(run[model]) for model in run]))
                
                f.write("\n")

def write_dict_to_file(_dict, path, prefix):

    with open(prefix + os.path.sep + path, "w") as f:
        sorted_keys = sorted(_dict.keys())
        for k in sorted_keys:
            f.write("{}: \n".format(k))
            for el in _dict[k]:
                f.write("{}\n".format(",".join([str(i) for i in el])))
            f.write("\n")

def get_optimal_runs(path, prefix):

    all_conditions = playback.crawl_results(path, use_caching=USE_CACHING)
    sorted_keys = sorted(all_conditions.keys())
    
    optimals = {}
    optimals10 = {}
    optimals20 = {}
    others = {}
    for i, cond in enumerate(sorted_keys):
        optimals[cond] = []
        optimals10[cond] = []
        optimals20[cond] = []
        others[cond] = []
        for j, (env, targets, agent, true_goal) in enumerate(all_conditions[cond]):
            number_actions = agent.non_dud_actions()
            number_optimal_actions = env.compute_distance(agent.start_pos, true_goal)
            if (number_actions - number_optimal_actions) == 0: 
                optimals[cond].append((agent.id, number_actions))
            elif (number_actions - number_optimal_actions) < number_optimal_actions*0.1: #10% means, 5% of directly corrected errors, as each error increased actions by 2
                optimals10[cond].append((agent.id, number_actions))
            elif (number_actions - number_optimal_actions) < number_optimal_actions*0.2: #20% means, 10% of directly corrected errors, as each error increased actions by 2
                optimals20[cond].append((agent.id, number_actions))
            else:
                others[cond].append((agent.id, number_actions))


    with open(prefix + os.path.sep + "percentages_optimal{}_T{}.csv".format(SURPRISE, MODEL_THRESHOLD), "w") as f:
        f.write("Condition; Percent Optimal; Percent +10 optimal; Percent +20 optimal; Percent near Optimal; Others\n")
        for cond in sorted_keys:
            f.write("{}; {:.4f}; {:.4f}; {:.4f}; {:.4f}; {:.4f}\n".format(cond, len(optimals[cond])/len(all_conditions[cond]), 
                                            len(optimals10[cond])/len(all_conditions[cond]), 
                                            len(optimals20[cond])/len(all_conditions[cond]),
                                            (len(optimals[cond])+len(optimals10[cond])+len(optimals20[cond]))/len(all_conditions[cond]),
                                            len(others[cond])/len(all_conditions[cond])))

    with open(prefix + os.path.sep + "numbers_optimal{}_T{}.csv".format(SURPRISE, MODEL_THRESHOLD), "w") as f:
        f.write("Condition; Number Optimal; Number +10 optimal; Number +20 optimal; Number near Optimal; Others; Number Total\n")
        for cond in sorted_keys:
            f.write("{}; {}; {}; {}; {}; {}; {}\n".format(cond, len(optimals[cond]), 
                                            len(optimals10[cond]), 
                                            len(optimals20[cond]),
                                            (len(optimals[cond])+len(optimals10[cond])+len(optimals20[cond])),
                                            len(others[cond]),
                                            len(all_conditions[cond])))

    write_dict_to_file(optimals, "optimals{}_T{}.csv".format(SURPRISE, MODEL_THRESHOLD), prefix)
    write_dict_to_file(optimals10, "optimals10{}_T{}.csv".format(SURPRISE, MODEL_THRESHOLD), prefix)
    write_dict_to_file(optimals20, "optimals20{}_T{}.csv".format(SURPRISE, MODEL_THRESHOLD), prefix)
    write_dict_to_file(others, "others{}_T{}.csv".format(SURPRISE, MODEL_THRESHOLD), prefix)


def prepare_completion_table(path, prefix):
    all_conditions = playback.crawl_results(path, use_caching=USE_CACHING)
    sorted_keys = sorted(all_conditions.keys())

    s = "Maze\tC1V1\tC1V2\tC2V1\tC2V2\tC3V1\tC3V2"
    cur_map = 0

    for cond in sorted_keys:
        matchObj = re.match("condMap(\d{1,})_C(\d{1,})_V(\d{1,})", cond)
        if matchObj:
            map_n = matchObj.group(1)
            c_n = matchObj.group(2)
            v_n = matchObj.group(3)

        if map_n != cur_map:
            s += "\n" +str(map_n)
            cur_map = map_n
        
        s += "\t"+str(len(all_conditions[cond]))

    with open(prefix + os.path.sep + "completions_table.csv", "w") as f:
        f.write(s)

def prepare_number_steps_csv(path, prefix):
    all_conditions = playback.crawl_results(path, use_caching=USE_CACHING)
    sorted_keys = sorted(all_conditions.keys())

    cur_map = 0
    opt1 = 0
    opt2 = 0
    
    # Construct header
    s = "Maze\toptV1\toptV2\tC1V1\tC1V2\tC2V1\tC2V2\tC3V1\tC3V2"
    for cond in sorted_keys:
        matchObj = re.match("condMap(\d{1,})_C(\d{1,})_V(\d{1,})", cond)
        if matchObj:
            map_n = matchObj.group(1)
            c_n = matchObj.group(2)
            v_n = matchObj.group(3)

        num_runs = len(all_conditions[cond])
        avg = 0

        s1_buf = 0
        # number_optimal_actions 
        for j, (env, targets, agent, true_goal) in enumerate(all_conditions[cond]):
            avg += agent.non_dud_actions()
            # if number_optimal_actions_v1 is None:
        if v_n == "1":
            opt1 = env.compute_distance(agent.start_pos, true_goal)
            avg1 = avg/num_runs
        else:
            opt2 = env.compute_distance(agent.start_pos, true_goal)
            avg2 = avg/num_runs

        if map_n != cur_map and opt2 != 0: 
            s += "\n" +str(map_n) + "\t{:2.2g}\t{:2.2g}".format(opt1,opt2)
            cur_map = map_n
            
        
        if opt2 != 0:
            s+= "\t{:.2f}".format((avg1-opt1)/opt1*100).rstrip('0').rstrip('.') + "\t{:.2f}".format((avg2-opt2)/opt2*100).rstrip('0').rstrip('.')
            opt2 = 0

    print("Writing file")
    with open(prefix + os.path.sep + "optimality.csv", "w") as f:
        f.write(s)

def prepare_timings_csv(prefix, timings):
    for model in timings:
        s = "Participant ID;Variant;Timings\n"
        for variant in timings[model]:
            for p_id, times in timings[model][variant]:
                s += "{};{};{}\n".format(p_id, variant, times)

        with open(prefix + os.path.sep + "timings{}_T{}_{}.csv".format(SURPRISE, MODEL_THRESHOLD, model), "w") as f:
            f.write(s)

def prepare_switches_csv(prefix, results_switching):

    s = "Participant ID;Variant;Final Results;Swiches\n"
    for variant in results_switching:
        for p_id in results_switching[variant]:
            all_swiches = results_switching[variant][p_id][1][1]
            if len(all_swiches) == 0:
                final_res = "TWG"
            else:
                final_res = all_swiches[-1][1]
            s += "{};{};{};{}\n".format(p_id, variant, final_res, all_swiches)

    with open(prefix + os.path.sep + "switches{}_T{}.csv".format(SURPRISE, MODEL_THRESHOLD), "w") as f:
        f.write(s)

def prepare_beliefs(results, targets, true_perm):
    perms = list(itertools.permutations([info["symbol"] for info in targets.values()]))
    true_goal_dist = {",".join(perm): 0 for perm in perms}
    true_goal_dist[",".join(true_perm)] = 1

    desire_prior = {info["symbol"]: 1/len(targets) 
                                    for info in targets.values()}
    goal_belief_priors = {perm: 1/len(perms) for perm in perms}
    world_belief_priors = {method: 0.5 
                            for method in ["TrueBelief", "FreeSpace"]} 

    true_world_belief = {"TrueBelief": 1, "FreeSpace": 0}

    initial_belief = {"desire": dict(desire_prior),
                    "goal": dict(goal_belief_priors),
                    "world": dict(world_belief_priors)}
    
    beliefs = {}
    for m in results:
        initial_belief = {"desire": dict(desire_prior),
                    "goal": dict(goal_belief_priors),
                    "world": dict(world_belief_priors)}
        beliefs[m] = results[m][1]
        if m == "Switching":
            beliefs[m] = beliefs[m][2]
            initial_belief["goal"] = dict(true_goal_dist)
            initial_belief["world"] = dict(true_world_belief)
        else:
            # Other models need to be rearranged from separate lists for each
            # belief, to a list of dictionaries containing all beliefs
            tmp = []
            if m == "TWG": 
                initial_belief["goal"] = dict(true_goal_dist)
                initial_belief["world"] = dict(true_world_belief)
                for desire_b in beliefs[m]:
                    tmp.append({"desire": dict(desire_b),
                            "goal":  dict(true_goal_dist),
                            "world": dict(true_world_belief)})
            if m == "TG":
                initial_belief["goal"] = dict(true_goal_dist)
                initial_belief["world"] = dict(world_belief_priors)
                for desire_b, world_b in zip(*beliefs[m]):
                    tmp.append({"desire": dict(desire_b),
                            "goal":  dict(true_goal_dist),
                            "world": dict(world_b)})
            if m == "TW":
                initial_belief["world"] = {"TrueBelief": 1, "FreeSpace": 0}
                for desire_b, goal_b in zip(*beliefs[m]):
                    tmp.append({"desire": dict(desire_b),
                            "goal":  dict(goal_b),
                            "world": {"TrueBelief": 1, "FreeSpace": 0}})
            if m == "Full BToM":
                for desire_b, goal_b, world_b in zip(*beliefs[m]):
                        tmp.append({"desire": dict(desire_b),
                                    "goal":  dict(goal_b),
                                    "world": dict(world_b)})
            beliefs[m] = tmp
        # Insert initial belief!
        beliefs[m].insert(0, initial_belief)
    return beliefs

def evaluate_single_experiment(path, name, prefix, write_beliefs=False):
    env, targets, agent, true_goal = playback.load_experiment(path) 

    results, true_perm = rate_single_episode(env, targets, agent, true_goal)
    results["Switching"] = rate_single_episode_switching(env, targets, agent, true_goal)

    model_switchings = results["Switching"][1][1]
    switching_indices = [model_switch[2]-1 for model_switch in model_switchings]

    s = "Step;{};Switches\n".format(";".join(results.keys()))

    for i in range(agent.non_dud_actions()):
        t = ";".join(["{:.6f}".format(results[m][-1][i]) for m in results.keys()])
        s += "{};{}".format(i+1, t)
        if i < len(switching_indices):
            s += ";{}".format(switching_indices[i])
        else:
            s += ";nan"
        
        s += "\n"

    with open(prefix + os.path.sep + "scoreList" + name + ".csv", "w") as f:
        f.write(s)

    beliefs = prepare_beliefs(results, targets, true_perm)
    if write_beliefs:
        for m in beliefs:
            s = "Step;Desires;Goal Beliefs;World Beliefs\n"
            for i, el in enumerate(beliefs[m]):
                s += "{};{};{};{}\n".format(i, el["desire"], el["goal"], el["world"])
            
            with open(prefix + os.path.sep + "beliefs_{}.csv".format(m), "w") as f:
                f.write(s)

    return beliefs


if __name__ == "__main__":
    import sys, os
    # Path towards the result files, assuming they are organized 
    # as they are published
    path = os.path.join(os.path.dirname(__file__), "../Participant_data")

    ## Compute ratings for the four basic models:
    results = rate_entire_episodes(path)

    # Path prefix to store the results in, the folder needs to exist, as the
    # functions will not check for it.
    prefix = "../rawData"

    # # Write results to csv files for further processing.
    prepare_results_csv(prefix, results)
    prepare_switches_csv(prefix, results["Switching"])

    compute_timings = True
    if compute_timings:

        USE_CACHING = False
        ## Extract only the timings from the switching results
        timings_switching = {}
        # Make sure we get switching times that do not benefit from caching of 
        # other models!
        results_switching = rate_entire_episodes(path, models={"Switching":True})["Switching"]

        for variant in results_switching:
            timings_switching[variant] = [(p_id, run_res[1][0]) for p_id, run_res in results_switching[variant].items()]

        means = {}
        stds = {}

        # print("Timings switching: ", timings_switching)

        timings = {"Switching": timings_switching}

        # Reset env_cache for next model
        playback.env_store = {}
        timings_TWG = get_timings(path, model_name="TWG")
        # Reset env_cache for next model
        playback.env_store = {}
        timings_TW = get_timings(path, model_name="TW")
        # Reset env_cache for next model
        playback.env_store = {}
        timings_TG = get_timings(path, model_name="TG")
        # Reset env_cache for next model
        playback.env_store = {}
        timings_NA = get_timings(path, model_name="NA")

        timings["TWG"] = timings_TWG
        timings["TW"] = timings_TW
        timings["TG"] = timings_TG
        timings["Full"] = timings_NA

        prepare_timings_csv(prefix, timings)

